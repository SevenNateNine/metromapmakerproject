/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileReader;
import java.util.LinkedList;
import javafx.scene.image.Image;
import javafx.scene.shape.Line;
import javax.json.JsonString;
import javax.json.stream.JsonParser;
import jdk.nashorn.internal.parser.JSONParser;
import mmm.data.MetroElement;
import mmm.data.MetroImage;
import mmm.data.MetroLabel;
import mmm.data.MetroLine;
import mmm.data.MetroLineSegment;
import mmm.data.MetroStation;
import mmm.data.mmmData;

/**
 * Handles loading and saving of the files
 * @author natec
 * saving of the files
 * @author natec
 */
public class mmmFiles implements AppFileComponent{
    

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Nathaniel Chan
 */
    // FOR JSON LOADING
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_BG_IMAGE = "background_image";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_RADX = "radius x";
    static final String JSON_RADY = "radius y";
    static final String JSON_THICKNESS = "thickness";
    
    //IMAGE
    static final String JSON_PATH = "image_path";
    
    //FONT
    static final String JSON_SIZE = "size";
    static final String JSON_STYLE = "style";
    static final String JSON_BOLD = "bold";
    static final String JSON_ITALIC = "italic";
    
    static final String JSON_LABEL = "label";
    static final String JSON_NAME = "name";
    static final String JSON_LINES = "lines";
    static final String JSON_STATIONS = "stations";
    static final String JSON_CIRCULAR = "circular";
    static final String JSON_COLOR = "color";
    static final String JSON_STATION_NAMES = "station_names";
    static final String JSON_IMAGES = "images";
    static final String JSON_LABELS = "labels";
    
    
    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";
    
 
    /**
     * Saves user work
     * 
     * @param data 
     *      The data class of the application
     * 
     * @param filePath 
     *      Path name where to save file to
     * 
     * @throws IOException 
     *      Thrown should there be an error writing out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	mmmData dataManager = (mmmData)data;
        
        //BACKGROUND COLOR
        Color bgColor = dataManager.getBackgroundColor();
        JsonObject bgColorJson = makeJsonColorObject(bgColor);
        String bgImagePath = dataManager.getBackgroundImagePath();
        String fileName = dataManager.getFileName();
        
        //BUILD JSON OBJECTS TO SAVE
        JsonArrayBuilder lineArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder stationArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder labelArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder imageArrayBuilder = Json.createArrayBuilder();
        ObservableList<Node> elements = dataManager.getElements();
        for(Node node : elements){
            if(node instanceof MetroLine){
                MetroLine line = (MetroLine)node;
                String name = line.toString();
                boolean isCircular = line.isCircular();
                double lineThickness = line.getLineThickness();
                JsonObject colorJson =
                        makeJsonColorObject((Color)((MetroLine) node).getLineColor());
                JsonArray stationListJson = 
                        makeJsonStationListObject(line.getStations());
                JsonObject lineJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_CIRCULAR, isCircular)
                        .add(JSON_THICKNESS, lineThickness)
                        .add(JSON_COLOR, colorJson)
                        .add(JSON_STATION_NAMES, stationListJson).build();
                lineArrayBuilder.add(lineJson);
            }
            if(node instanceof MetroStation){
                Shape shape = (Shape)node;
                MetroStation metroShape = ((MetroStation)shape);
                if(!metroShape.isLineEnd()){
                    String name = metroShape.toString();
                    double x = metroShape.getX();
                    double y = metroShape.getY();
                    double radiusX = metroShape.getRadiusX();
                    double radiusY = metroShape.getRadiusY();
                    JsonObject colorJson =
                        makeJsonColorObject((Color)(metroShape.getColor()));
                    JsonObject label =
                        makeJsonLabelObject(metroShape.getStationLabel());
                    JsonObject stationJson = Json.createObjectBuilder()
                            .add(JSON_NAME, name)                         
                            .add(JSON_X, x)
                            .add(JSON_Y, y)
                            .add(JSON_RADX, radiusX)
                            .add(JSON_RADY, radiusY)
                            .add(JSON_COLOR, colorJson)
                            .add(JSON_LABEL, label).build();
                    stationArrayBuilder.add(stationJson);
                }
            }
            if(node instanceof MetroImage){
                Shape shape = (Shape)node;
                MetroImage metroImage = ((MetroImage)shape);
                double x = metroImage.getX();
                double y = metroImage.getY();
                String imagePath = metroImage.getImagePath();
                JsonObject imageJson = Json.createObjectBuilder()
                        .add(JSON_PATH, imagePath)
                        .add(JSON_X, x)
                        .add(JSON_Y, y).build();
                imageArrayBuilder.add(imageJson);
            }         
            if(node instanceof MetroLabel){
                Shape shape = (Shape)node;
                MetroLabel metroLabel= ((MetroLabel)shape);
                if(metroLabel.getMasterStation() == null){
                    double x = metroLabel.getX();
                    double y = metroLabel.getY();
                    JsonObject label = makeJsonLabelObject(metroLabel);
                    JsonObject labelJson = Json.createObjectBuilder()
                            .add(JSON_X, x)
                            .add(JSON_Y, y)
                            .add(JSON_LABEL, label)
                            .build();
                    labelArrayBuilder.add(labelJson);
                }
            }
        }
            JsonArray linesArray = lineArrayBuilder.build();
            JsonArray stationsArray = stationArrayBuilder.build();
            JsonArray imagesArray = imageArrayBuilder.build();
            JsonArray labelsArray = labelArrayBuilder.build();
            
            JsonObject dataManagerJSO = Json.createObjectBuilder()
                    .add(JSON_NAME, fileName)
                    .add(JSON_BG_COLOR, bgColorJson)
                    .add(JSON_BG_IMAGE, bgImagePath)
                    .add(JSON_LINES, linesArray)
                    .add(JSON_STATIONS, stationsArray)
                    .add(JSON_IMAGES, imagesArray)
                    .add(JSON_LABELS, labelsArray)
                    .build();
            
            Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
            StringWriter sw = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(sw);
            jsonWriter.writeObject(dataManagerJSO);
            jsonWriter.close();

            OutputStream os = new FileOutputStream(filePath + ".json");
            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(dataManagerJSO);
            String prettyPrinted = sw.toString();
            PrintWriter pw = new PrintWriter(filePath + ".json");
            pw.write(prettyPrinted);
            pw.close(); 
    }
    
    private JsonObject makeJsonColorObject(Color color) {
	JsonObject colorJson = Json.createObjectBuilder()
		.add(JSON_RED, color.getRed())
		.add(JSON_GREEN, color.getGreen())
		.add(JSON_BLUE, color.getBlue())
		.add(JSON_ALPHA, color.getOpacity()).build();
	return colorJson;
    }
    
    private JsonArray makeJsonStationListObject(LinkedList<MetroStation> metroStations){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for(MetroStation s : metroStations){
                    arrayBuilder.add(s.toString());
                }
                JsonArray stationsArray = arrayBuilder.build();
        return stationsArray;
    }
    
    private JsonObject makeJsonLabelObject(MetroLabel label){
        JsonObject colorJson =
                        makeJsonColorObject((Color)(label.getFill()));
        JsonObject labelJson = Json.createObjectBuilder()
		.add(JSON_NAME, label.toString())
                .add(JSON_SIZE, label.getFontSize())
                .add(JSON_STYLE, label.getFontStyle())
                .add(JSON_BOLD, label.isBold())
                .add(JSON_ITALIC, label.isItalic())
                .add(JSON_COLOR, colorJson).build();
	return labelJson;
    }
      
    /**
     * Loads data from the file path parameter and 
     * then updates the workspace for editing

     * @param data 
     *      Where the file is loaded into
     * 
     * @param filePath 
     *      Path where to load file from
     * @throws IOException 
     *      Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        //CLEAR OLD DATA                
        mmmData dataManager = (mmmData)data;
        dataManager.resetData();
        
        //LOAD FILE WITH ALL DATA
        JsonObject json = loadJSONFile(filePath);
        
        if(json != null){
            System.out.println("File Retrieved : " + json.getString(JSON_NAME, filePath));
            
        }
        //System.out.println(filePath);
        
        
        //LOAD BACKGROUND COLOR               
        Color bgColor = loadColor(json, JSON_BG_COLOR);
        dataManager.setBackgroundColor(bgColor);
        //LOAD BACKGROUND IMAGE
        Image backImage = new Image(getDataAsString(json, JSON_BG_IMAGE));
        dataManager.setBackgroundImage(backImage, getDataAsString(json, JSON_BG_IMAGE));
               
        //LOAD STUFF
        //5 LOOPS, LINES, STATIONS, SEGMENTS, LABELS (not related labels), IMAGES       
        JsonArray jsonStationArray = json.getJsonArray(JSON_STATIONS);
        //JsonArray jsonLabelArray = json.getJsonArray(JSON_ARRAY);
        for(int i = 0; i < jsonStationArray.size(); i++){
            JsonObject jsonStation = jsonStationArray.getJsonObject(i);
            Shape label = loadLabel(jsonStation, JSON_LABEL);
            Shape station = loadStation(jsonStation);           
           //dataManager.addStation(station, )
        }
        
        JsonArray jsonLineArray = json.getJsonArray(JSON_LINES);
        for(int i = 0; i < jsonLineArray.size(); i++){
            JsonObject jsonLine = jsonLineArray.getJsonObject(i);
            Shape line = loadLine(jsonLine);
            dataManager.addElement(line);
        }
        
        /*
        .add(JSON_NAME, name)                         
                            .add(JSON_X, x)
                            .add(JSON_Y, y)
                            .add(JSON_RADX, radiusX)
                            .add(JSON_RADY, radiusY)
                            .add(JSON_COLOR, colorJson)
                            .add(JSON_LABEL, label).build();
        */
        
    }
    /**
     * Loads color of the shape from file
     * @param json
     *      The object that has the color
     * @param colorToGet
     *      The color of the object
     * @return 
     *      The color being loaded
     */
    private Color loadColor(JsonObject json, String colorToGet) {
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    
    private Shape loadStation(JsonObject jsonElement){
        Color lineColor = loadColor(jsonElement, JSON_COLOR);
        String name = getDataAsString(jsonElement, JSON_NAME);
        
        Shape shape = new MetroStation();
        return shape;
    }
    
    private Shape loadLabel(JsonObject jsonElement, String need){
        JsonObject jsonLabel = jsonElement.getJsonObject(need);
        String text = getDataAsString(jsonLabel, JSON_NAME);
        Shape label =  new MetroLabel(text);
        return label;
    }
    
    private Shape loadLine(JsonObject jsonElement){
        Color lineColor = loadColor(jsonElement, JSON_COLOR);
        String name = getDataAsString(jsonElement, JSON_NAME);
        double thickness = getDataAsDouble(jsonElement, JSON_THICKNESS);
        
        Shape shape = new MetroLine(name, lineColor, thickness);
        
        ((MetroLine)shape).setCircular(getDataAsBoolean(jsonElement, JSON_CIRCULAR));
        /*
        elementArray = elements;
        this.line = newLine;
        this.lineSegment = newLineSegment;
        this.lineEndOne = newLineEnd;
        this.lineEndTwo = secondNewLineEnd;
        this.lineLabelOne = newLineLabel;
        this.lineLabelTwo = secondNewLineLabel;
        */
        //((MetroLine)shape).set
        
        //Go through loop of stations and add them to line if
        //Names match
        
        
        /*
        .add(JSON_NAME, name)
                        .add(JSON_CIRCULAR, isCircular)
                        .add(JSON_THICKNESS, lineThickness)
                        .add(JSON_COLOR, colorJson)
                        .add(JSON_STATION_NAMES, stationListJson)
                        .add(JSON_LABEL, lineEndOneJson)
                        .add(JSON_LABEL, lineEndTwoJson).build();
        */
        
        
        return shape;
    }
    
    private String getDataAsString(JsonObject json, String dataName){
        JsonValue value = json.get(dataName);
        JsonString string = (JsonString)value;
        return string.getString();
    }
    
    private boolean getDataAsBoolean(JsonObject json, String dataName){
        boolean bool = (boolean)json.getBoolean(dataName);
        return bool;
    }
    
    private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    /**
     * Loads shape from file
     * @param jsonShape
     *      The shape being loaded
     * @return 
     *      The shape being loaded
     */
    private Shape loadElement(JsonObject jsonElement) {
	return null;
    }       

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        mmmData dataManager = (mmmData)data;
        String fileName = dataManager.getFileName();
        //BUILD JSON OBJECTS TO SAVE
        JsonArrayBuilder lineArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder stationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Node> elements = dataManager.getElements();
        for(Node node : elements){                       
            if(node instanceof MetroLine){
                
                MetroLine line = (MetroLine)node;
                String name = line.toString();
                boolean isCircular = line.isCircular();
                JsonObject colorJson = 
                        makeJsonColorObject((Color)((MetroLine) node).getLineColor());
                JsonArray stationListJson = 
                        makeJsonStationListObject(line.getStations());
                
                JsonObject lineJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_CIRCULAR, isCircular)
                        .add(JSON_COLOR, colorJson)
                        .add(JSON_STATION_NAMES, stationListJson).build();
                lineArrayBuilder.add(lineJson);
            }
            if(node instanceof MetroStation){
                Shape shape = (Shape)node;
                MetroStation metroShape = ((MetroStation)shape);
                if(!metroShape.isLineEnd()){
                    String name = metroShape.toString();
                    double x = metroShape.getX();
                    double y = metroShape.getY();
                    JsonObject stationJson = Json.createObjectBuilder()
                            .add(JSON_NAME, name)
                            .add(JSON_X, x)
                            .add(JSON_Y, y).build();
                    stationArrayBuilder.add(stationJson);
                }
            }                       
            //JsonObject dataManagerJSO =                        
        }
        JsonArray linesArray = lineArrayBuilder.build();
        JsonArray stationsArray = stationArrayBuilder.build();
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_NAME, fileName)
                .add(JSON_LINES, linesArray)
                .add(JSON_STATIONS, stationsArray)
                .build();
        
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();
        
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();        
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

