/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Handles the MetroLabel object's creation and other properties.
 * @author natec
 */
public class MetroLabel extends Text implements MetroElement{
    double startX;
    double startY;
    double fontSize;
    String fontStyle;
    boolean isBold;
    boolean isItalic;
    boolean isEnd;
    double rotateValue;
    
    String labelLocation;
    MetroStation masterStation;
    
    /**
     * Constructor for the MetroLabel object class
     */
    public MetroLabel(String text) {
        labelLocation = "TOP_RIGHT";
        rotateValue = 0;
        this.setOpacity(1.0);
        
        fontStyle = "Monospaced";
        fontSize = 15;
        
        this.setFont(Font.font (fontStyle, fontSize));
        this.setText(text);
        
    }
    
    /**
     * Returns current state of label object
     * @return 
     *      State of MetroLabel
     */
    @Override
    public mmmState getStartingState(){
        return null;
    }
    
    /**
     * Creates starting coordinate of MetroLabel object
     * @param x
     *      x coordinate of starting point
     * @param y 
     *      y coordinate of starting point
     */
    @Override
    public void start(int x, int y){
        startX = x;
        startY = y;
        setX(startX);
        setY(startY);
    }
    
    /**
     * Drags element to desired destination.
     * @param x
     *      Current x coordinate of element
     * @param y 
     *      Current y coordinate of element
     */
    @Override
    public void drag(int x, int y){
        double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
        setX(newX);
        setY(newY);
        startX = x;
        startY = y;
    }
    
    /**
     * Sets the width and height of element relative to its parameters
     * @param x
     *      Subtracts from width of label
     * @param y 
     *      Subtracts from height of label
     */
    @Override
    public void size(int x, int y){
        
    }
    
    /**
     * Sets the location and size of the label element
     * @param initX
     *      The new x coordinate of the label element
     * @param initY
     *      The new y coordinate of the label element
     * @param initWidth
     *      The new width of the label element
     * @param initHeight 
     *      The new height of the label element
     */
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight){
        
    }
    
    /**
     * Returns the element type of the label element
     * @return 
     *      The type of the label element
     */
    
    //MAY BE REDUNDENT (refer to MetroLine)
    public String getElementType(){
        return LABEL;
    }
    
    /**
     * Returns the font size of the text in the label element
     * @return 
     *      The font size
     */
    public double getFontSize(){
        return fontSize;
    }
    
    /**
     * Returns the font style of the text in the label element
     * @return 
     *      The font style of the label element
     */
    public String getFontStyle(){
        return fontStyle;
    }
    
    /**
     * Sets the font size of the label element
     * @param i 
     *      The new size of the label element's text's font
     */
    public void setFontSize(double i){
        fontSize = i;
        refreshFont();
    }
    
    public void setFontStyle(String s){
        fontStyle = s;
        refreshFont();
    }
    
    /**
     * Refreshes the font when changes are made to the label element.
     */
    public void refreshFont(){
        if(isBold && isItalic){
            this.setFont(Font.font(fontStyle, FontWeight.BOLD, FontPosture.ITALIC, fontSize));
        }
        else if(isBold){
            this.setFont(Font.font(fontStyle, FontWeight.BOLD, fontSize));
        }
        else if(isItalic){
            this.setFont(Font.font(fontStyle, FontPosture.ITALIC, fontSize));
        }
        else{
            this.setFont(Font.font(fontStyle, fontSize));
        }
    }
    
    /**
     * Bolds the text of the label element
     */
    public void boldText(){
        if(isBold){     //unBold
            if(isItalic){
                this.setFont(Font.font(fontStyle, FontPosture.ITALIC, fontSize));
            }
            else{
                this.setFont(Font.font(fontStyle, fontSize));
            }
            isBold = false;
        }
        else if(!isBold){   //Bold
            if(isItalic){
                this.setFont(Font.font(fontStyle, FontWeight.BOLD, FontPosture.ITALIC, fontSize));
            }
            else{
                this.setFont(Font.font(fontStyle, FontWeight.BOLD, fontSize));
            }
            isBold = true;
        }
    }
    
    /**
     * Italicizes the text of the label element
     */
    public void italicizeText(){
        if(isItalic){   //unItalic
            if(isBold){
                this.setFont(Font.font(fontStyle, FontWeight.BOLD, fontSize));
            }
            else{
                this.setFont(Font.font(fontStyle, fontSize));
            }
            isItalic = false;
        }
        else if(!isItalic){     //Italic
            if(isBold){
                this.setFont(Font.font(fontStyle, FontWeight.BOLD, FontPosture.ITALIC, fontSize));
            }
            else{
                this.setFont(Font.font(fontStyle, FontPosture.ITALIC, fontSize));
            }
            isItalic = true;
        }
    }
    
    /**
     * Creates an identical object of this object
     * @return 
     *      The new cloned MetroLabel object
     */
    public MetroLabel clone(){
        return null;
    }

    
    @Override
    public String getShapeType() {
        return LABEL;
    }

    @Override
    public double getWidth() {
         return this.getLayoutBounds().getWidth();
    }

    @Override
    public double getHeight() {
        return this.getLayoutBounds().getHeight();
    }
    
    public String toString(){
        return this.getText();
    }
    
    public MetroStation getMasterStation(){
        return masterStation;
    }
    
    public void setMasterStation(MetroStation masterStation){
        this.masterStation = masterStation;
    }
    
    public boolean isEnd(){
        return isEnd;
    }
    
    public void setEnd(boolean end){
        isEnd = end;
    }

    public void setLocation(double x, double y){
        setX(x);
        setY(y);
        startX = x;
        startY = y;
    }
    
    public boolean isBold(){
        return isBold;
    }
    
    public boolean isItalic(){
        return isItalic;
    }
    
    public void rotateLabel(double value){
        if(rotateValue > 360){
            rotateValue = 0;
        }
        else if(rotateValue < 0){
            rotateValue = 360;
        }
        else{
            rotateValue += value;          
        }
        setRotate(rotateValue);
    }
    
    public void moveLabelClockWise(){
        switch(labelLocation){
            case "TOP_RIGHT" :  
                labelLocation = "BOTTOM_RIGHT";
                break;
            case "BOTTOM_RIGHT" : 
                labelLocation = "BOTTOM_LEFT";
                break;
            case "BOTTOM_LEFT" :
                labelLocation = "TOP_LEFT";
                break;
            case "TOP_LEFT" : 
                labelLocation = "TOP_RIGHT";
                break;
        }
        moveLabel();
    }
    
    public void moveLabelCounterClockWise(){
        switch(labelLocation){
            case "TOP_RIGHT" :  
                labelLocation = "TOP_LEFT";
                break;
            case "BOTTOM_RIGHT" : 
                labelLocation = "TOP_RIGHT";
                break;
            case "BOTTOM_LEFT" :
                labelLocation = "BOTTOM_RIGHT";
                break;
            case "TOP_LEFT" : 
                labelLocation = "BOTTOM_LEFT";
                break;
        }
        moveLabel();
    }
    
    public void moveLabel(){
        double radiusX = getMasterStation().getRadiusX();
        double radiusY = getMasterStation().getRadiusY();
        double stationX = getMasterStation().getCenterX();
        double stationY = getMasterStation().getCenterY();
        switch(labelLocation){
            case "TOP_RIGHT" :  
                if(rotateValue == 90 || rotateValue == 270){
                    this.setLocation(stationX + radiusX - this.getWidth()/2.5, stationY - radiusY);
                }else{
                    this.setLocation(stationX + radiusX, stationY - radiusY);
                }
                break;
            case "BOTTOM_RIGHT" : 
                if(rotateValue == 90 || rotateValue == 270){
                    this.setLocation(stationX + radiusX - this.getWidth()/2.5, stationY + radiusY);
                }else{
                    this.setLocation(stationX + radiusX, stationY + radiusY);
                }             
                break;
            case "BOTTOM_LEFT" :
                if(rotateValue == 90 || rotateValue == 270){
                    this.setLocation(stationX - radiusX - this.getWidth()/1.5, stationY + radiusY);
                }else{
                    this.setLocation(stationX - radiusX - this.getWidth(), stationY + radiusY);
                }
                break;
            case "TOP_LEFT" : 
                if(rotateValue == 90 || rotateValue == 270){
                    this.setLocation(stationX - radiusX - this.getWidth()/1.5, stationY - radiusY);
                }else{
                    this.setLocation(stationX - radiusX - this.getWidth(), stationY - radiusY);
                }
                break;
        }
    }

}
