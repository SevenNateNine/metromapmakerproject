/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import djf.AppTemplate;
import djf.components.AppDataComponent;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javafx.collections.FXCollections;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import static mmm.data.MetroElement.*;
import static mmm.data.mmmState.*;
import mmm.gui.mmmWorkspace;
import mmm.transactions.*;

/**
 * Handles the majority of the methods for the application
 * @author natec
 */
public class mmmData implements AppDataComponent{
    String fileName;
    ObservableList<Node> elements;
    Color backgroundColor;
    Image backgroundImage;
    
    Shape newElement, secondNewElement, thirdNewElement, fourthNewElement,
            fifthNewElement, sixthNewElement, seventhNewElement;
    Shape selectedElement;
    
    Color currentFillColor;
    Color currentOutlineColor;
    double currentBorderWidth;
    mmmState state;
    AppTemplate app;
    Effect highlightedEffect;
    ImageView imageView;
    
    Image elementImage;
    Shape storedElement;
    String comboStation;
    ArrayList<String> comboStringList;
    MetroStation comboStationObject;
    String comboLine;
    String backgroundImagePath;
    
    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;
    static jTPS jTPS = new jTPS();
    
    /**
     * The constructor for the MetroMapMakerData class
     * @param initApp 
     *      The application this class is serving
     */
    public mmmData(AppTemplate initApp){
        app = initApp;
        fileName = "";
        
        newElement = null;
        selectedElement = null;
        
        currentFillColor = Color.web(WHITE_HEX);
        backgroundColor = Color.WHITE;
        backgroundImagePath = "";
        
        //THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(15);
	highlightedEffect = dropShadowEffect;
        comboStringList = new ArrayList<String>();
    }
    
    
    public ObservableList<Node> getElements(){
        return elements;
    }
    
    public Color getBackgroundColor(){
        return backgroundColor;
    }
    
    public Color getCurrentFillColor(){
        return null;
    }
    
    public double getCurrentBorderWidth(){
        return 0;
    }
    
    public void setShapes(ObservableList<Node> initElements){
        elements = initElements;
    }
    
    public void setBackgroundColor(Color initBackgroundColor){
        backgroundColor = initBackgroundColor;
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getBackgroundCanvas();
        BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
        Background background = new Background(fill);  
        
        jTPS_Transaction transaction = 
                new ChangeBackgroundColor(canvas, canvas.getBackground(), background);
        jTPS.addTransaction(transaction);
    }
    
    public void setBackgroundImage(Image image, String filePath){
        backgroundImage = image;
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getImageCanvas();

        BackgroundImage backImage = new BackgroundImage(image, null, null, null, null);
        Background background = new Background(backImage);
        
        backgroundImagePath = filePath;
        
        jTPS_Transaction transaction = 
                new ChangeBackgroundColor(canvas, canvas.getBackground(), background);
        jTPS.addTransaction(transaction);             
    }
    
    public String getBackgroundImagePath(){
        return backgroundImagePath;
    }
    
    public void setCurrentFillColor(Color initColor){
        ((MetroStation)((mmmWorkspace)app.getWorkspaceComponent()).getMetroStationComboBox().getValue()).setColor(initColor);
    }
    
    public void setCurrentLineThickness(double newBorderWidth, double originalBorderWidth, MetroLine selectedLine){
        jTPS_Transaction transaction = new ChangeLineThickness(newBorderWidth, originalBorderWidth, selectedLine);
            jTPS.addTransaction(transaction);
    }
    
    public void setCurrentStationRadius(double newRadius, double oldRadius, MetroStation metroStation){
        jTPS_Transaction transaction = new ChangeStationRadius(newRadius, oldRadius, metroStation);
        jTPS.addTransaction(transaction);
    }
    
    /**
     * Removes the selected element.
     */
    public void removeStation(){
        if((((MetroElement)selectedElement).getShapeType()).equals(STATION)){
            MetroStation selectedStation = (MetroStation)selectedElement;
            MetroLabel stationLabel = selectedStation.getStationLabel();
            
            if(selectedStation.inLine){
                this.removeStationFromLine(selectedStation);
            }
            
            jTPS_Transaction transaction = new RemoveStation(
                    elements,
                    selectedStation,
                    stationLabel
            );
            jTPS.addTransaction(transaction);
            
            ((mmmWorkspace)app.getWorkspaceComponent()).removeStationHelper(selectedStation);
        }
    }
    
    public void removeLine(MetroLine line){
            jTPS_Transaction transaction = new RemoveLine(
                    elements,
                    line,
                    line.getLineSegments(),
                    line.getLineEndOne(),
                    line.getLineEndTwo(),
                    line.getLineEndOne().getStationLabel(),
                    line.getLineEndTwo().getStationLabel()
            );
            jTPS.addTransaction(transaction);             
            ((mmmWorkspace)app.getWorkspaceComponent()).removeLineHelper(line);
            this.stationsToFront();
    }
    
    /**
     * Resets all data in this class
     */
    public void resetData(){
        setState(SELECTING_ELEMENT);
	newElement = null;
	selectedElement = null;

	// INIT THE COLORS
	currentFillColor = Color.web(WHITE_HEX);
	currentOutlineColor = Color.web(BLACK_HEX);
	
	elements.clear();
	((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().clear();
    }
    
    /**
     * Selects newly sized elements
     */
    public void selectSizedElement(){
        
    }
    
    public Shape selectTopShape(int x, int y){
        Shape shape = getTopShape(x, y);
            if(!(shape instanceof Line)){
                if(shape != null){
                ((MetroElement)shape).start(x,y);
                }
                if(selectedElement != null){
                    unhighlightElement(selectedElement);
                }
                if(shape != null && !((MetroElement)shape).getShapeType().equals(LINE_SEGMENT)){
                    highlightElement(shape);
                    mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
                    workspace.loadSelectedShapeSettings(shape);
                }
                selectedElement = shape;
                if(shape == selectedElement){
                    return shape;
                }                  
            }   
            return shape;
    }
    
    public Shape getTopShape(int x, int y){
        for(int i = elements.size() - 1; i >= 0; i--){
            Shape shape = (Shape)elements.get(i);
            if(shape.contains(x,y)){
                return shape;
            }

        }
        return null;
    }
    
    /**
     * Stops highlighting the parameter element
     * @param element 
     *      The element ceasing to be highlighted
     */
    public void highlightElement(Shape element){
        element.setEffect(highlightedEffect);
    }
    
    /**
     * Highlights the parameter element
     * @param element 
     *      The element to be highlighted
     */
    public void unhighlightElement(Shape element){
        selectedElement.setEffect(null);
    }
    
    public void addStationToLine(MetroStation station){       
        MetroLineSegment closestLineSegment = null;
        double closestLineDistance = 10000000000000000.0;
        //FINDS CLOSEST LINE SEGMENT
        for(Node s : elements){
            if(s instanceof MetroLineSegment){
                MetroLineSegment currentSegment = (MetroLineSegment)s;
                //Checks if the currentSegment is equal to the selectedLine
                if(currentSegment.getMasterLine().toString().equals(comboLine)){       
                    double x = 
                            Math.pow((station.centerXProperty().get() - currentSegment.getX()), 2);
                    double y =
                            Math.pow((station.centerYProperty().get() - currentSegment.getY()), 2);
                    double distance = Math.sqrt(x + y);
                    
                    if(distance < closestLineDistance){
                        closestLineSegment = currentSegment;
                        closestLineDistance = distance;
                    }
                }
            }
        }
        if(closestLineSegment == null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("There are no lines!");
            alert.setContentText("Please create a line before adding stations!");
            alert.showAndWait();
        }
        else{                                  
            
            jTPS_Transaction transaction = new AddStationsToLine(
                elements, 
                closestLineSegment,
                station
            );        
            jTPS.addTransaction(transaction);
            
            this.stationsToFront();
        }
    }
    
    public void removeStationFromLine(MetroStation station){
        MetroLine selectedLine = this.getLineFromElements(comboLine);
        if(selectedLine == null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("There are no lines!");
            alert.setContentText("Please create a line before adding stations!");
            alert.showAndWait();
        }
        else{
            MetroLineSegment[] segment = new MetroLineSegment[2];
            //Goes through the station's segments and find the two segments 
            //(Should only be two) that match the selectedLine.
            int j = 0;
            for(int i = 0; i < station.getLineSegments().size(); i++){               
                MetroLineSegment currentSegment = station.getLineSegments().get(i);
                if((currentSegment.getMasterLine()).toString().equals(selectedLine.toString())){
                    segment[j] = station.getLineSegments().get(i);
                    j++;                    
                }
            }
            station.setInLine(false);
            MetroStation[] otherEnd = new MetroStation[2];
            
            //Goes through both saves the station that is not the station being removed.
            j = 0;
            for(int i = 0; i < segment.length; i++){
                if(segment[i].getFirstLineEnd().toString().equals(station.toString())){
                    otherEnd[j] = segment[i].getSecondLineEnd();
                    j++;
                }
                else if(segment[i].getSecondLineEnd().toString().equals(station.toString())){
                    otherEnd[j] = segment[i].getFirstLineEnd();
                    j++;
                }
            }
            
            //Remove Segment 1, connect Segment 0 to Segment 1's end that's not the station
            MetroLineSegment lineSegmentNew = new MetroLineSegment(
                    selectedLine.getLineColor(),
                    otherEnd[0],
                    otherEnd[1],
                    selectedLine.getLineThickness(),
                    selectedLine
            );
          
            jTPS_Transaction transaction = new RemoveStationsFromLine(
                elements, 
                segment[0],
                segment[1],
                lineSegmentNew,
                station
            );        
            jTPS.addTransaction(transaction);
            
            this.stationsToFront();
        }
    }
    
    
    /**
     * Creates a new MetroLineSegment object
     * @param x
     *      The starting x coordinate of the object
     * @param y 
     *      The starting y coordinate of the object
     */
    public void startNewMetroLine(String text, Color color, double thick){
        MetroLine newMetroLine = new MetroLine(text, color, thick);
        MetroStation lineEndOne = newMetroLine.getLineEndOne();
        MetroStation lineEndTwo = newMetroLine.getLineEndTwo();
        MetroLabel lineLabelOne = lineEndOne.getStationLabel();
        MetroLabel lineLabelTwo = lineEndTwo.getStationLabel();

        newElement = newMetroLine;
        secondNewElement = newMetroLine.getLineSegments().getFirst();
        thirdNewElement = lineEndOne;
        fourthNewElement = lineEndTwo;
        fifthNewElement = lineLabelOne;
        sixthNewElement = lineLabelTwo;
        jTPS_Transaction transaction = new AddLine(
                elements, 
                newElement, 
                secondNewElement,
                thirdNewElement, 
                fourthNewElement, 
                fifthNewElement, 
                sixthNewElement
        );        
        jTPS.addTransaction(transaction);
            
        ((mmmWorkspace)app.getWorkspaceComponent()).addLineHelper(newMetroLine);
        
        this.stationsToFront();
    }
    
    /**
     * Creates a new MetroLabel object
     * @param x
     *      The starting x coordinate of the object
     * @param y 
     *      The starting y coordinate of the object
     */
    public void startNewMetroLabel(String name){
        MetroLabel label = new MetroLabel(name);
        label.setLocation(600, 600);
        newElement = label;
        jTPS_Transaction transaction = new AddElement(elements, newElement);
        jTPS.addTransaction(transaction);
        
    }
    
    public void rotateMetroLabel(MetroStation station){
        MetroLabel label = station.getStationLabel();
        jTPS_Transaction transaction = new RotateLabel(label);
        jTPS.addTransaction(transaction);
    }
    
    public void moveMetroLabel(MetroStation station){
        MetroLabel label = station.getStationLabel();
        jTPS_Transaction transaction = new MoveLabel(label);
        jTPS.addTransaction(transaction);
    }
    
    /**
     * Creates a new MetroStation object
     * @param x
     *      The starting x coordinate of the object
     * @param y 
     *      The starting y coordinate of the object
     */
    public void startNewMetroStation(String text, Color color){                 
        String testText = text;
        int j = 1;
        for(Node s : this.getElements()){
            if(s instanceof MetroStation){
                MetroStation currentStation = (MetroStation)s;
                           //Tests if elementText is equal to any other line name
                           if(currentStation.toString().equals(text)){
                               //If so add a 1 to it
                               text = testText + " " + j;
                               j++;
                           }
            }
        }
        //Makes MetroStation's MetroLabel, BEFORE new MetroStation
        MetroLabel stationLabel = new MetroLabel(text); 
        //Then makes the newStation
        MetroStation newStation = new MetroStation(color, stationLabel); 
        newElement = newStation;
        secondNewElement = stationLabel;
        jTPS_Transaction transaction = new AddStation(elements, newElement, secondNewElement);
        jTPS.addTransaction(transaction);
        
        //Adds new station to MetroStation comboBox
        ((mmmWorkspace)app.getWorkspaceComponent()).addStationHelper(newStation);
        
        //Should do this stuff directly
        this.setComboStation(text);
        this.setComboStationObject(newStation);
        
        //Ensures that MetroStations are always infront of MetroLines
        this.stationsToFront();
    }
    
    public void addImage(Image image, File file){
        MetroImage newImage = new MetroImage(image);
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        newImage.setImagePath(file.getPath());
        newImage.start(
                (int)workspace.getCanvas().getHeight()/3, 
                (int)workspace.getCanvas().getWidth()/3
        );
        newImage.setLocation(
                (int)workspace.getCanvas().getHeight()/2, 
                (int)workspace.getCanvas().getWidth()/3
        );
        newElement = newImage; 
        jTPS_Transaction transaction = new AddImage(elements, newElement);
        jTPS.addTransaction(transaction);
    }
    
    public MetroLine editMetroLine(MetroLine currentLine, Color color, String text){
        currentLine.setLineColor(color);
        currentLine.setLineName(text);
        return currentLine;
    }
    
    /**
     * Bolds the text of the selected element
     */
    public void boldText(){
        if(((MetroElement)selectedElement).getShapeType().equals(LABEL)){
            jTPS_Transaction transaction = new BoldText(((MetroLabel)selectedElement));
            jTPS.addTransaction(transaction);
        }
    }
    
    /**
     * Italicizes the text of the selected element
     */
    public void italicizeText(){
        if(((MetroElement)selectedElement).getShapeType().equals(LABEL)){
            jTPS_Transaction transaction = new ItalicizeText(((MetroLabel)selectedElement));
            jTPS.addTransaction(transaction);
        }
    }
    
    /**
     * Changes the font size of the selected element
     * @param i 
     *      The new font size of the selected element
     */
    public void changeFontSize(double i){
        if(((MetroElement)selectedElement).getShapeType().equals(LABEL)){
            jTPS_Transaction transaction = new ChangeFontSize(((MetroLabel)selectedElement), i);
            jTPS.addTransaction(transaction);
        }
    }
    
    /**
     * Changes the font style of the selected element
     * @param s 
     *      The new font style of the selected element
     */
    public void changeFontStyle(String s){
        if(((MetroElement)selectedElement).getShapeType().equals(LABEL)){
            jTPS_Transaction transaction = new ChangeFontStyle(((MetroLabel)selectedElement), s);
            jTPS.addTransaction(transaction);
        }
    }
    
    public void changeFontColor(Color s){
        if(((MetroElement)selectedElement).getShapeType().equals(LABEL)){
            jTPS_Transaction transaction = new ChangeFontColor(((MetroLabel)selectedElement), s);
            jTPS.addTransaction(transaction);
        }
    }
    
    public void toggleGrid(boolean isOn){
            mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
            BorderPane canvas = workspace.getMasterCanvas();
            jTPS_Transaction transaction = new ToggleGrid(canvas, elements, isOn);
            jTPS.addTransaction(transaction);
    }
    
    public void snapToGrid(){
        jTPS_Transaction transaction = new SnapToGrid((MetroStation)selectedElement);
        jTPS.addTransaction(transaction);
    }
    
    public void mapSizeChange(boolean increase){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        BorderPane canvas = workspace.getMasterCanvas();
        
        jTPS_Transaction transaction = new ChangeMapSize(canvas, increase);
        jTPS.addTransaction(transaction);
    }
    
    public void mapZoom(boolean zoomIn){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        ScrollPane canvas = workspace.getParentCanvas();
        
        jTPS_Transaction transaction = new ChangeMapZoom(canvas, zoomIn);
        jTPS.addTransaction(transaction);
    }
    
    public void navigateViewport(String direction){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        ScrollPane canvas = workspace.getParentCanvas();
        
        double vValue = canvas.getVvalue();
        double hValue = canvas.getHvalue();
        
        switch(direction){
            case "UP":
                canvas.setVvalue(vValue-.03);
                break;
            case "DOWN":
                canvas.setVvalue(vValue+.03);
                break;
            case "RIGHT":
                canvas.setHvalue(hValue+.03);
                break;
            case "LEFT":
                canvas.setHvalue(hValue-.03);
                break;
        }
    }
    
   /**
    * Creates a new element
    */ 
    public void initNewElement(){
        if(selectedElement != null){
            unhighlightElement(selectedElement);
            selectedElement = null;
        }
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        
        
        
        //jTPS_Transaction transaction = new AddElement(text, color);
        //jTPS.addTransaction(transaction);
    }
    
    public Shape getNewElement(){
        return newElement;
    }
    
    public Shape getSelectedElement(){
        return selectedElement;
    }
    
    public void setSelectedElement(Shape initSelectedElement){
        selectedElement = initSelectedElement;
    }
    
    public void addElement(Shape elementToAdd){
        elements.add(elementToAdd);
    }
    
    public void removeElement(MetroElement elementToRemove){
        elements.remove(elementToRemove);
    }
    
    public void removeSelectedElement(){
        jTPS_Transaction transaction = new RemoveElement(elements, selectedElement);
        jTPS.addTransaction(transaction);
    }
    
    public jTPS getjTPS(){
        return jTPS;
    }
    
    public mmmState getState(){
        return state;
    }
    
    public void setState(mmmState initState){
        state = initState;
    }
    
    public boolean isInState(mmmState testState){
        if(state.equals(testState)){
            return true;
        }
        return false;
    }
    
    /**
     * Undoes the last action of the user
     */
    public void undo(){
        jTPS.undoTransaction();
    }
    
    /**
     * Performs the last action undone by the user.
     */
    public void redo(){
        jTPS.doTransaction();
    }
    
    /**
     * Displays about information
     */
    public void about(){
        Alert about = new Alert(Alert.AlertType.INFORMATION);      
            about.setTitle("About us!");
            about.setHeaderText(null);
                
            about.setContentText("App Name: MetroMapMaker \n"
                    + "Developer: Nathaniel Chan \n"
                    + "Year of Creation: 2017 \n"
            );                         
            about.showAndWait();                
      
        about.setGraphic(imageView);
    }
            
    public void setComboStation(String value){
        comboStation = value;
        comboStationObject = getComboStationObject();
    }
    
    public void setComboLine(String value){
        comboLine = value;
    }
    
    public String getComboStation(){
        return comboStation;
    }
    
    public String getComboLine(){
        return comboLine;
    }
    
    public MetroLine getComboLineObject(){
        for(int i = 0; i < elements.size(); i++){
            MetroElement currentElement = (MetroElement)elements.get(i);
            if(currentElement.getShapeType().equals(LINE)){
                MetroLine currentLine = (MetroLine)currentElement;
                if(currentLine.toString().equals(comboLine)){
                    return currentLine;
                }
            }
        }
        return null;
    }
    
    public MetroStation getComboStationObject(){
        for(Node s : elements){
            if(s instanceof MetroStation){
                MetroStation currentStation = (MetroStation)s;
                if(currentStation.toString().equals(comboStation)){
                    return currentStation;
                }
            }
        }
        return null;
    }
    
    public void setComboStationObject(MetroStation object){
        comboStationObject = object;
    }
    
    public MetroLine getLineFromElements(String lineName){
        for(int i = 0; i < elements.size(); i++){
            MetroElement currentElement = (MetroElement)elements.get(i);
            if(currentElement.getShapeType().equals(LINE)){
                MetroLine currentLine = (MetroLine)currentElement;
                if(currentLine.toString().equals(lineName)){
                    return currentLine;
                }
            }
        }
        return null;
    }
    
    public void stationsToFront(){
        ObservableList<Node> stationList = FXCollections.observableArrayList();
        ObservableList<Node> gridList = FXCollections.observableArrayList();
        ObservableList<Node> newList = FXCollections.observableArrayList();
        
        for(Node s : elements){
            if(s instanceof MetroStation){
                stationList.add(s);
            }
            else if(s instanceof Line){
                gridList.add(s);
            }
            else{
                    newList.add(s);
            }
        }
                      
        for(Node s : gridList){
            newList.add(s);
        }
        for(Node s : stationList){
            newList.add(s);
        }
        
        elements.clear();
        
        for(Node s : newList){
            elements.add(s);
        }
    }
    
    public void deselectStations(){
        for(Node s : elements){
            if(s instanceof MetroLabel){
                ((MetroLabel)s).setVisible(true);
            }
            if(s instanceof MetroStation){
                if(((MetroStation)s).isLineEnd){
                    ((MetroStation)s).setFill(null);
                }
            }
        }
    }
    
    public void bfs(MetroStation station){
        for(Node s : elements){
            if(s instanceof MetroStation){
                ((MetroStation)s).setVisited(false);
                ((MetroStation)s).getPath().clear();
            }
        }
        Queue<MetroStation> queue = new LinkedList<MetroStation>();
        
        station.getPath().add(station);
        queue.add(station);
        station.setVisited(true);
        while(!queue.isEmpty()){
            MetroStation parent = queue.poll();
            
            for(MetroStation stationNeighbor : parent.getMasterLine().getStations()){
                if(!stationNeighbor.isVisited){
                    stationNeighbor.setVisited(true);
                    
                    LinkedList<MetroStation> parentPath = (LinkedList<MetroStation>)parent.getPath().clone();
                    parentPath.add(stationNeighbor);
                    stationNeighbor.setPath(parentPath);
                    
                    queue.add(stationNeighbor);
                }
            }
        }
    }
    
    //FILE RELATED CONTENT
        public void setFileName(String newName){
            fileName = newName;
        }
    
        public String getFileName(){
           return fileName; 
        }
        
        public void addStation(Shape metroStation, Shape metroLabel){
            elements.add(metroStation);
            elements.add(metroLabel);
        }
        
        public void addLine(){
            
        }
        
}
