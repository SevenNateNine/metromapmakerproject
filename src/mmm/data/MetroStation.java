/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.util.LinkedList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.StrokeLineJoin;

/**
 * Handles the station objects' creation and properties.
 * @author natec
 */
public class MetroStation extends Ellipse implements MetroElement{
    LinkedList<MetroLineSegment> connectedLines;
    MetroLine masterLine;
    MetroLabel stationLabel;
    double startCenterX;
    double startCenterY;
    double radiusX;
    double radiusY;
    Color color;
    boolean isLineEnd;
    boolean inLine;
    
    //FOR SEARCHING
    LinkedList<MetroStation> path;
    boolean isVisited;
    
    public MetroStation(){
        
    }
    
    /**
     * The construct of the MetroStation element class
     */
    public MetroStation(Color color, MetroLabel stationLabel){
        isVisited = false;
        path = new LinkedList<MetroStation>();
        
        connectedLines = new LinkedList<MetroLineSegment>();     
        /*
        ellipse.setCenterX(50.0f);
        ellipse.setCenterY(50.0f);
        ellipse.setRadiusX(50.0f);
        ellipse.setRadiusY(25.0f);
        */      
        this.color = color;
        startCenterX = 400;
        startCenterY = 400;
        this.setCenterX(400);
        this.setCenterY(400);
        radiusX = 20;
        radiusY = 20;
        this.setRadiusX(20);
        this.setRadiusY(20);
        this.setSmooth(true);
        this.setFill(color);
        this.setStroke(Color.BLACK);
        this.setStrokeLineJoin(StrokeLineJoin.ROUND);
        this.setStrokeWidth(2);
        this.stationLabel = stationLabel;
        inLine = false;
        
        stationLabel.setMasterStation(this);
        stationLabel.setLocation(
                startCenterX + radiusX, 
                startCenterY - radiusY
        );
        
        masterLine = null;
        
        //stationLabel.translateXProperty().bind(this.centerXProperty());
        //stationLabel.translateYProperty().bind(this.centerYProperty().add(-35));
    }
    
    public mmmState getStartingState(){
        return mmmState.CREATING_STATION;
    }
    
    /**
     * Sets the starting coordinate of the MetroStation element
     * @param x
     *      The initial x coordinate of the MetroStation object
     * @param y 
     *      The initial y coordinate of the MetroStation object
     */
    public void start(int x, int y){
        startCenterX = x;
	startCenterY = y;
        setCenterX(startCenterX);
        setCenterY(startCenterY);
        stationLabel.start(x, y);
        stationLabel.moveLabel();
    }
    
    /**
     * Enables the MetroStation element to be draged
     * @param x
     *      The current x coordinate of the MetroStation object
     * @param y 
     *      The current y coordinate of the MetroStation object
     */
    public void drag(int x, int y){
        double diffX = x - startCenterX;
	double diffY = y - startCenterY;
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	startCenterX = x;
	startCenterY = y;
        stationLabel.drag(x, y);
        stationLabel.moveLabel();
    }
    
    /**
     * Sets the size of the MetroStation element based on the parameters
     * @param x
     *      Helps set the new width of the element
     * @param y    
     *      Helps sets the new height of the element
     */
    public void size(int x, int y){
        double width = x - startCenterX;
	double height = y - startCenterY;
	double centerX = startCenterX + (width / 2);
	double centerY = startCenterY + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	setRadiusX(width / 2);
	setRadiusY(height / 2);	
    }
    
    public double getX(){
        return getCenterX() - getRadiusX();
    }
    
    public double getY(){
        return getCenterY() - getRadiusY();
    }
    
    public double getWidth(){
        return getRadiusX() * 2;
    }
    
    public double getHeight(){
        return getRadiusY() * 2;
    }
    
    /**
     * Sets a new location and size for the object
     * @param initX
     *      The new x coordinate of the object
     * @param initY
     *      The new y coordinate of the object
     * @param initWidth
     *      The new width of the object
     * @param initHeight 
     *      The new height of the object
     */
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight){
        setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadiusX(initWidth/2);
	setRadiusY(initHeight/2);
    }
    
    public void setLocation(double initX, double initY){
        setCenterX(initX + (getWidth()/2));
	setCenterY(initY + (getHeight()/2));
        getStationLabel().moveLabel();
    }
    
    public String getElementType(){
        return STATION;
    }
    
    /**
     * Creates a new MetroStation object identical to this one
     * @return 
     *      The new MetroStation object
     */
    public MetroStation clone(){
        MetroStation clonedStation = new MetroStation(color, stationLabel);
        clonedStation.setLocationAndSize(getX()+5, getY()+5, getWidth(), getHeight());
        return clonedStation;
    }

    @Override
    public String getShapeType() {
        return STATION;
    }
    
    public MetroLabel getStationLabel(){
        return stationLabel;
    }
    
    public boolean isLineEnd(){
        return isLineEnd;
    }
    
    public void setIsLineEnd(boolean isLineEnd){
        this.isLineEnd = isLineEnd;
        this.stationLabel.setEnd(isLineEnd);
    }
    
    public String toString(){
        return stationLabel.toString();
    }
    
    public void addLineSegments(MetroLineSegment newLineSegment){
        connectedLines.add(newLineSegment);
    }
    
    
    public LinkedList<MetroLineSegment> getLineSegments(){
        return connectedLines;
    }
    
    public boolean inLine(){
        return inLine;
    }
    
    public void setInLine(boolean bool){
        inLine = bool;
    }
    
    public void setColor(Color color){
        this.color = color;
        this.setFill(color);
    }
    
    public Color getColor(){
        return color;
    }
    
    public boolean equals(MetroStation station){
        if(
                this.getColor().equals(station.getColor()) &&
                this.centerXProperty().get()==station.centerXProperty().get() &&
                this.centerYProperty().get()==station.centerYProperty().get() &&
                this.toString().equals(station.toString()) &&
                this.getLineSegments().equals(station.getLineSegments())
          )return true;
        return false;
    }
    
    //FOR SEARCHING YO
    public void setVisited(boolean value){
        this.isVisited = value;
    }
    
    public boolean isVisited(){
        return isVisited;
    }
    
    public LinkedList<MetroStation> getPath(){
        return path;
    }
    
    public void setPath(LinkedList<MetroStation> newPath){
        path = newPath;
    }
    
    public void setMasterLine(MetroLine line){
        masterLine = line;
    }
    
    public MetroLine getMasterLine(){
        return masterLine;
    }
}
