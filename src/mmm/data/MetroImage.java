/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author natec
 */
public class MetroImage extends Rectangle implements MetroElement{
    
    double startX;
    double startY;
    Image shapeImage;
    String imagePath;
    
    public MetroImage(Image image){
        shapeImage = image;      
        
        setX(0.0);
        setY(0.0);
        setWidth(shapeImage.getWidth());
        setHeight(shapeImage.getHeight());
        setOpacity(1.0);
        startX = 0.0;
        startY = 0.0;
        
        System.out.println("image constructer working!");
        
        this.setFill(new ImagePattern(image, 0, 0, 
                1, 1, true));
    }

    @Override
    public mmmState getStartingState() {
        return mmmState.SELECTING_ELEMENT;
    }

    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        setX(startX);
        setY(startY);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - startX;  //Offset
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
        setX(newX);
        setY(newY);
	startX = x;
	startY = y;
    }

    @Override
    public void size(int x, int y) {
        double width = x - getX();
	widthProperty().set(width);
	double height = y - getY();
	heightProperty().set(height);
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
	yProperty().set(initY);
	widthProperty().set(initWidth);
	heightProperty().set(initHeight);
    }
    
    public void setLocation(double x, double y){
        xProperty().set(x);
	yProperty().set(y);
    }

    @Override
    public String getShapeType() {
        return IMAGE;
    }
    
    public MetroImage clone(){
        MetroImage clonedImage = new MetroImage(shapeImage);
        clonedImage.setLocation(getX() + 5, getY() + 5);
        return clonedImage;
    }
    
    public Image getImage(){
        return this.shapeImage;
    }
    
    public void setImagePath(String filePath){
        this.imagePath = filePath;
    }
    
    public String getImagePath(){
        return imagePath;
    }
}
