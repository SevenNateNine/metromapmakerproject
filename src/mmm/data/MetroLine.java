/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.util.LinkedList;
import javafx.scene.paint.Color;
import javafx.scene.shape.QuadCurve;
import mmm.gui.mmmWorkspace;

/**
 *
 * @author natec
 */
public class MetroLine extends QuadCurve implements MetroElement{
    
    LinkedList<MetroLineSegment> metroLineSegments;
    LinkedList<MetroStation> metroStations;
    MetroStation lineEnd;
    MetroStation lineEndTwo;
    Color lineColor;
    String lineName;
    Boolean circular;
    double lineThickness;
    
    public MetroLine(){
        
    }
    
    /**
     * Constructor
     * @param text
     * @param color 
     */
    public MetroLine(String text, Color color, double thick){
        circular = false;
        metroLineSegments = new LinkedList<MetroLineSegment>();
        metroStations = new LinkedList<MetroStation>();
        lineColor = color;
        lineName = text;
        //Line Labels for Line Ends
        MetroLabel newLabel = new MetroLabel(text);
        MetroLabel secondNewLabel = new MetroLabel(text);
        //Line Ends
        lineEnd = new MetroStation(null, newLabel);
        lineEndTwo = new MetroStation(null, secondNewLabel);
        
        lineThickness = thick;
        
        MetroLineSegment newLine = new MetroLineSegment(color, lineEnd, lineEndTwo, lineThickness, this);
        
        lineEnd.addLineSegments(newLine);
        lineEndTwo.addLineSegments(newLine);
        metroLineSegments.add(newLine);
            //Make sure Line Ends are not visible
            lineEnd.setStroke(null);
            lineEndTwo.setStroke(null);
                      
        lineEnd.setIsLineEnd(true);
        lineEndTwo.setIsLineEnd(true);
        
            
    }
    
    public void addStation(MetroStation newStation){
        metroStations.add(newStation);
    }
    
    public void removeStation(MetroStation station){
        metroStations.remove(station);
    }
    
    

    @Override
    public mmmState getStartingState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void start(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void drag(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getY() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShapeType() {
        return LINE;
    }
    
    public MetroStation getLineEndOne(){
        return lineEnd;
    }
    
    public MetroStation getLineEndTwo(){
        return lineEndTwo;
    }
    
    public LinkedList<MetroLineSegment> getLineSegments(){
        return metroLineSegments;
    }
    
    public LinkedList<MetroStation> getStations(){
        return metroStations;
    }
    
    public boolean isCircular(){
        return circular;
    }
    
    public void setCircular(boolean newValue){
        circular = newValue;
    }
    
    public Color getLineColor(){
        return lineColor;
    }
    
    public void setLineColor(Color color){
        lineColor = color;
        //Change all the line segment colors now
        for(int i = 0; i < metroLineSegments.size(); i++){
            metroLineSegments.get(i).setLineColor(color);
        }
    }
    
    
    public void setLineThickness(double thick){
        lineThickness = thick;
        for(int i = 0; i < metroLineSegments.size(); i++){
            metroLineSegments.get(i).setStrokeWidth(thick);
        }
    }
    
    public double getLineThickness(){
        return lineThickness;
    }
    
    public void setLineName(String newName){
        lineName = newName;
        this.getLineEndOne().getStationLabel().setText(newName);
        this.getLineEndTwo().getStationLabel().setText(newName);
    }
    
    public String toString(){
        return lineName;
    }
    
    
    public double get(){
        return lineThickness;
    }

}
