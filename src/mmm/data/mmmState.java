/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 * Contains the state of the user's actions
 * @author natec
 */
public enum mmmState {
    SELECTING_ELEMENT, 
    DRAGGING_ELEMENT, 
    CREATING_LINE,
    CREATING_STATION,
    DRAGGING_NOTHING,
    CREATING_LABEL,
    ADD_STATION_MODE,
    REMOVE_STATION_MODE
}
