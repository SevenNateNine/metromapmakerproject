/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.QuadCurve;
import javafx.scene.shape.StrokeLineCap;

/**
 * Handles the MetroLineSegment object's creation and other properties.
 * @author natec
 */
public class MetroLineSegment extends Line implements MetroElement{
    
    MetroStation lineEndOne;
    MetroStation lineEndTwo;
    MetroLine masterLine;
    double startX;
    double startY;
    double endX;
    double endY;
    Color color;
    double segmentThickness;
    
    /**
     * Default Constructor
     */
    public MetroLineSegment(){
        
    }
    
    /**
     * Construct for the MetroLine object class
     * @param color
     */
    public MetroLineSegment(Color color, 
            MetroStation lineEndOne, 
            MetroStation lineEndTwo, 
            double thick, 
            MetroLine masterLine
    ){
        this.masterLine = masterLine;
        startX = 400;
        startY = 400;
        endX = 1000;
        endY = 400;
        segmentThickness = thick;
        //Default size of line
        //Where the line  begins
        this.setStartX(400);
        this.setStartY(400);
        //Where the line ends
        this.setEndX(1000);
        this.setEndY(400);
        //Where the curve of the line is (in the middle, but no curve)
        //this.setControlX(null);
        //this.setControlY(null); 
        this.color = color;        
        //Color of the line
        this.setStroke(color);
        //Enables stroke/sets width of stroke to 10
        this.setSmooth(true);
        this.setStrokeWidth(thick);
        //Ensures the ends of the stroke are round
        this.setStrokeLineCap(StrokeLineCap.ROUND);
        //Sets the color of the line.
        this.setFill(null);  //Background?
        
        this.lineEndOne = lineEndOne;
        this.lineEndTwo = lineEndTwo;    
        
         //Binds LineEnds to end of Lines       
        
        lineEndOne.setLocationAndSize(
                startX, 
                startY, 
                lineEndOne.getRadiusX(), 
                lineEndOne.getRadiusY()
        );
        lineEndTwo.setLocationAndSize(
                endX, 
                endY, 
                lineEndTwo.getRadiusX(), 
                lineEndTwo.getRadiusY()
        );
        
        lineEndOne.getStationLabel().setLocation(
                (int)lineEndOne.getCenterX() + (int)lineEndOne.getRadiusX(), 
                (int)lineEndOne.getCenterY() - (int)lineEndOne.getRadiusY()
        );
        lineEndTwo.getStationLabel().setLocation(
                (int)lineEndTwo.getCenterX() + (int)lineEndTwo.getRadiusX(),
                (int)lineEndTwo.getCenterY() - (int)lineEndTwo.getRadiusY()
        ); 
        
        this.startXProperty().bind(lineEndOne.centerXProperty());
        this.startYProperty().bind(lineEndOne.centerYProperty());
        this.endXProperty().bind(lineEndTwo.centerXProperty());
        this.endYProperty().bind(lineEndTwo.centerYProperty());
    }
    
    public MetroLineSegment(Color color, 
            MetroStation lineEndOne, 
            MetroStation lineEndTwo, 
            MetroLine masterLine,
            double newStartX,
            double newStartY,
            double newEndX,
            double newEndY
            ){
        this.masterLine = masterLine;
        startX = newStartX;
        startY = newStartY;
        endX = newEndX;
        endY = newEndY;
        //Default size of line
        //Where the line  begins
        this.setStartX(newStartX);
        this.setStartY(newStartY);
        //Where the line ends
        this.setEndX(newEndX);
        this.setEndY(newEndY);
        //Where the curve of the line is (in the middle, but no curve)
        //this.setControlX(null);
        //this.setControlY(null); 
        this.color = color;        
        //Color of the line
        this.setStroke(color);
        //Enables stroke/sets width of stroke to 10
        this.setSmooth(true);
        this.setStrokeWidth(5);
        //Ensures the ends of the stroke are round
        this.setStrokeLineCap(StrokeLineCap.ROUND);
        //Sets the color of the line.
        this.setFill(null);  //Background?
        
        this.lineEndOne = lineEndOne;
        this.lineEndTwo = lineEndTwo;    
                        
        lineEndOne.setLocationAndSize(
                startX, 
                startY, 
                lineEndOne.getRadiusX(), 
                lineEndOne.getRadiusY()
        );
        lineEndTwo.setLocationAndSize(
                endX, 
                endY, 
                lineEndTwo.getRadiusX(), 
                lineEndTwo.getRadiusY()
        );
        
        lineEndOne.getStationLabel().setLocation(
                (int)lineEndOne.getCenterX(), 
                (int)lineEndOne.getCenterY()
        );
        lineEndTwo.getStationLabel().setLocation(
                (int)lineEndTwo.getCenterX(),
                (int)lineEndTwo.getCenterY()
        );  
        
        //Binds LineEnds to end of Lines
        this.startXProperty().bind(lineEndOne.centerXProperty());
        this.startYProperty().bind(lineEndOne.centerYProperty());
        this.endXProperty().bind(lineEndTwo.centerXProperty());
        this.endYProperty().bind(lineEndTwo.centerYProperty());
    }
    
    @Override
    public mmmState getStartingState(){
        return mmmState.CREATING_LINE;
    }
    
    /**
     * Sets the starting coordinates when creating the MetroLineSegment object
     * @param x
     *      The x coordinate of the startX variable
     * @param y 
     *      The y coordinate of the startY variable
     */
    
    @Override
    public void start(int x, int y){
        startX = x;
        startY = y;
    }
    
    /**
     * Allows the MetroLineSegment class to be dragged
     * @param x
     *      The current x coordinate of the MetroLineSegment object
     * @param y 
     *      The current y coordinate of the MetroLineSegment object
     */
    @Override
    public void drag(int x, int y){ //Parameters are cursor's coordinates
        /*
            double startDiffX = x - startX;  //Offset: Cursor's x coordinate minus Line's starting x coordinate
            double startDiffY = y - startY;  //Offset with y coordinate
            double endDiffX = x - endX + (endX - startX);   //Offset Cursor's x coordinate minus Line's Ending x coordinate + (distance between both points)
            double endDiffY = y - endY + (endY - startY);
            double controlDiffX = x - controlX; //Offset: Cursor's x coordinate - current controlX coordinate
            double controlDiffY = y - controlY;
            double newStartX = getStartX() + startDiffX;
            double newStartY = getStartY() + startDiffY;
            double newEndX = getEndX() + endDiffX;  
            double newEndY = getEndY() + endDiffY;
            double newControlX = getControlX() + controlDiffX;  //Adds offset to current control x coordinate
            double newControlY = getControlY() + controlDiffY;
            setStartX(newStartX);
            setStartY(newStartY);
            setEndX(newEndX);
            setEndY(newEndY);
            setControlX(newControlX);   
            setControlY(newControlY);
            startX = newStartX;
            startY = newStartY;
            endX = newEndX;
            endY = newEndY;
            controlX = newControlX;
            controlY = newControlY;               
        */
    }
    
    /**
     * Sets the size of the MetroLineSegment object
     * @param x
     *      The current x coordinate of the MetroLineSegment object
     * @param y 
     *      The current y coordinate of the MetroLineSegment object
     */
    
    public void setLocation(double x, double y, double x1, double y1){
        this.startXProperty().unbind();
        this.startYProperty().unbind();
        this.endXProperty().unbind();
        this.endYProperty().unbind();
        setStartX(x);
        setStartY(y);
        setEndX(x1);
        setEndY(y1);
        this.startXProperty().bind(lineEndOne.centerXProperty());
        this.startYProperty().bind(lineEndOne.centerYProperty());
        this.endXProperty().bind(lineEndTwo.centerXProperty());
        this.endYProperty().bind(lineEndTwo.centerYProperty());
    }
    
    /**
     * Sets the location and size of the MetroLineSegment object
     * @param initX
     *      The new x coordinate of the MetroLineSegment object
     * @param initY
     *      The new y coordinate of the MetroLineSegment object
     * @param initHeight
     *      The new Height of the MetroLineSegment object
     * @param initWidth 
     *      The new Width of the MetroLineSegment object
     */
    @Override
    public void setLocationAndSize(double initX, double initY, double initHeight, double initWidth){
        
    }  
    
    
    //MAY BE REDUNDENT DUE TO getShapeType()
    public String getElementType(){
        return LINE;
    }
    
    /**
     * Creates and returns a new MetroLineSegment object identical to this MetroLineSegment object
     * @return 
     *      The new MetroLineSegment object
     */
    @Override
    public MetroLineSegment clone(){
        MetroStation clonedEndOne = lineEndOne.clone();
        MetroStation clonedEndTwo = lineEndTwo.clone();
        MetroLineSegment clonedLine = new MetroLineSegment(color, clonedEndOne, clonedEndTwo, segmentThickness, masterLine);
        clonedLine.setLocation(getStartX()+5, getStartY()+5, 
                getEndX()+5, getEndY()+5); //Copies off to the side a bit.
        return clonedLine;
    }

    @Override
    public String getShapeType() {
        return LINE_SEGMENT;
    }

    @Override
    public double getX() {
        return (this.startXProperty().get() + this.endXProperty().get())/2;
    }

    @Override
    public double getY() {
        return (this.startYProperty().get() + this.endYProperty().get())/2;
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public MetroStation getFirstLineEnd(){
        return lineEndOne;
    }
    
    public void setFirstLineEnd(MetroStation newStation){
        lineEndOne = newStation;
    }
    
    public MetroStation getSecondLineEnd(){
        return lineEndTwo;
    }
    
    public void setSecondLineEnd(MetroStation newStation){
        lineEndTwo = newStation;
    }
    
    public String toString(){
        return lineEndOne.toString();
    }
    
    public void setLineColor(Color color){
        this.color = color;
        this.setStroke(color);
    }
    
    public Color getLineColor(){
        return color;
    }
    
    public void setLineThickness(double newThick){
        this.setStrokeWidth(newThick);
    }
    
    public double getLineThickness(){
        return this.strokeWidthProperty().getValue();
    }
    
    public MetroLine getMasterLine(){
        return masterLine;
    }
    
    public void setMasterLine(MetroLine newLine){
        masterLine = newLine;
    }    
    
    public void addStationToLine(MetroStation newStation, MetroLineSegment newLineSegment){
        //Sets it to midpoint of this line
        newStation.setLocation(this.getX(), this.getY());   
        newStation.addLineSegments(this);
        newStation.addLineSegments(newLineSegment);
               
        this.setSecondLineEnd(newStation);
        this.endXProperty().unbind();
        this.endYProperty().unbind();
        this.setEndX(this.getX());
        this.setEndY(this.getY());
        //Binds line ends to Station
        this.endXProperty().bind(newStation.centerXProperty());
        this.endYProperty().bind(newStation.centerYProperty());
        newLineSegment.startXProperty().bind(newStation.centerXProperty());
        newLineSegment.startYProperty().bind(newStation.centerYProperty());
               
        masterLine.getLineSegments().add(newLineSegment);
    }
    
    public void removeStationFromLine(MetroStation station, MetroLineSegment lineSegment){

        station.getLineSegments().remove(this);
        station.getLineSegments().remove(lineSegment);
        
        masterLine.getLineSegments().remove(lineSegment);
    }
}
