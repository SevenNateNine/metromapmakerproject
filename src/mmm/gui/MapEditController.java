/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import mmm.data.MetroElement;
import static mmm.data.MetroElement.*;
import mmm.data.MetroLine;
import mmm.data.MetroLineSegment;
import mmm.data.MetroStation;
import mmm.data.mmmData;
import mmm.data.mmmState;

/**
 * Made to process actions before they are performed
 * @author natec
 */
public class MapEditController {
    AppTemplate app;
    mmmData dataManager;
    MetroLine currentLine;
    Color elementColor;
    String elementText;
    boolean gridOn = false;
    
    /**
     * The constructor for the MapEditController class
     * @param initApp 
     *      The app that this class is serving
     */
    public MapEditController(AppTemplate initApp){
        app = initApp;
        dataManager = (mmmData)initApp.getDataComponent();
    }
    
    /**
     * Handles the response to color a line
     */
    public void processLineColorTool(){
        
    }
    
    /**
     * Handles the response to add a line to the canvas
     */
    public void processAddLineTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        
        //Scene scene = app.getGUI().getPrimaryScene();
        //scene.setCursor(Cursor.CROSSHAIR);  //Changes Crosshair
        
        //PROMPT
            Stage addLineStage = new Stage();
            addLineStage.setTitle("Creating a Line...");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);
                //ColorPicker
                Label colorFieldLabel = new Label("Color of Line: ");
                ColorPicker colorPicker = new ColorPicker();   
                colorPicker.setOnAction(e->{
                    elementColor = colorPicker.getValue();
                });
                HBox colorHBox = new HBox();
                colorHBox.getChildren().addAll(colorFieldLabel, colorPicker);
                //TextInput
                Label textFieldLabel = new Label("Name of Line: ");
                TextField textField = new TextField();               
                HBox textHBox = new HBox();
                textHBox.getChildren().addAll(textFieldLabel, textField);               
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction(e->{
                if(textField.getText() != null && !textField.getText().isEmpty()){
                   elementText = textField.getText();
                   String testText = elementText;                
                   //NO DUPLICATES
                   int j = 1;  
                   /*
                   for(int i = 0; i < dataManager.getElements().size(); i++){                               
                       if(){
                           MetroElement currentElement = (MetroElement) dataManager.getElements().get(i);
                            if(currentElement.getShapeType().equals(LINE)){
                                MetroLine currentLine = (MetroLine)currentElement;
                                //Tests if elementText is equal to any other line name
                                if(currentLine.toString().equals(elementText)){
                                    //If so add a 1 to it
                                    elementText = testText + " " + j;
                                    j++;
                                }
                            }
                       }
                   }
                   */
                   for(Node s : dataManager.getElements()){
                       if(s instanceof MetroLine){
                           MetroLine currentLine = (MetroLine)s;
                                //Tests if elementText is equal to any other line name
                                if(currentLine.toString().equals(elementText)){
                                    //If so add a 1 to it
                                    elementText = testText + " " + j;
                                    j++;
                                }
                       }
                   }
                   addLineStage.close();
                   dataManager.startNewMetroLine(elementText, elementColor, workspace.getLineSlider().getValue());
                }
                else{
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("No text detected!");
                    alert.setContentText("Please enter a line name.");
                    alert.showAndWait();
                }
            });
            cancelButton.setOnAction(e->{
                addLineStage.close();
            });
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));    
            GridPane.setMargin(textHBox, new Insets(10,0,0,15));
            GridPane.setMargin(colorHBox, new Insets(10,0,0,15));  
            GridPane.setConstraints(buttonHBox, 0, 2);
            GridPane.setConstraints(textHBox, 0, 0);
            GridPane.setConstraints(colorHBox,0, 1);
            root.getChildren().addAll(textHBox, colorHBox, buttonHBox);
            Scene addLine = new Scene(root, 325, 200);
            addLineStage.setScene(addLine);
            addLineStage.showAndWait();
        
        //END PROMPT
        
        //dataManager.startNewMetroLabel();
        
        //RELOADS WORKSPACE
        workspace.reloadWorkspace(dataManager);
    }
    
    public void processEditLineTool(MetroLine currentLine){  
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        //Checks if there is a value in the box
        if(currentLine == null){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No selected element detected!");
            alert.setContentText("Please select a line in the line combo box\n"
                    + "to be edited!");
            alert.showAndWait();
        }
        else{
            Stage editLineStage = new Stage();
            editLineStage.setTitle("Edit a Line...");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);
                //ColorPicker
                Label colorFieldLabel = new Label("Color of Line: ");
                ColorPicker colorPicker = new ColorPicker(); 
                colorPicker.setValue(currentLine.getLineColor());   //Current color of line
                colorPicker.setOnAction(e->{
                    elementColor = colorPicker.getValue();
                });
                HBox colorHBox = new HBox();
                colorHBox.getChildren().addAll(colorFieldLabel, colorPicker);
                //TextInput
                Label textFieldLabel = new Label("Name of Line: ");
                TextField textField = new TextField();  
                textField.setText(currentLine.toString());  //Current name of line
                HBox textHBox = new HBox();
                textHBox.getChildren().addAll(textFieldLabel, textField);               
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction((ActionEvent e)->{
                if(textField.getText() != null && !textField.getText().isEmpty()){
                   elementText = textField.getText();
                   String testText = elementText; 
                   //Check for Duplicates in ComboBox (Used to be in Elements)
                   int j = 1;       
                   if(!currentLine.toString().equals(elementText)){
                       for(int i = 0; i < workspace.getMetroLineComboBox().getItems().size(); i++){
                       if(workspace.getMetroLineComboBox().getItems().get(i).toString().equals(elementText)){
                           elementText = testText + " " + j;
                           j++;
                       }
                   }   
                   }               
                   editLineStage.close();                  
                   MetroLine newLine = 
                           dataManager.editMetroLine(currentLine, elementColor, elementText);
                   System.out.println("newLine Thickness : " + newLine.getLineThickness());
                   workspace.editLineHelper(newLine, currentLine);
                   workspace.updateEditLineButton();    //Updates LineColorButton
                   
                   
                }
                else{
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("No text detected!");
                    alert.setContentText("Please enter a line name.");
                    alert.showAndWait();
                }
            });
            cancelButton.setOnAction(e->{
                editLineStage.close();
            });
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));    
            GridPane.setMargin(textHBox, new Insets(10,0,0,15));
            GridPane.setMargin(colorHBox, new Insets(10,0,0,15));  
            GridPane.setConstraints(buttonHBox, 0, 2);
            GridPane.setConstraints(textHBox, 0, 0);
            GridPane.setConstraints(colorHBox,0, 1);
            root.getChildren().addAll(textHBox, colorHBox, buttonHBox);
            Scene addLine = new Scene(root, 325, 200);
            editLineStage.setScene(addLine);
            editLineStage.showAndWait();
        }
    }
    
    /**
     * Handles the response to remove a line from the canvas
     */
    public void processRemoveLineTool(MetroLine line){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        //Checks if there is a value in the box
        if(line == null){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No selected element detected!");
            alert.setContentText("Please select a line in the line combo box\n"
                    + "to be edited!");
            alert.showAndWait();
        }
        else{
            Stage removeLineStage = new Stage();
            removeLineStage.setTitle("Remove a Line...");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);        
                //Are you sure?
                Label confirmation = new Label("Are you sure you wish to \nremove the selected line?");
                confirmation.setAlignment(Pos.CENTER);
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction(e->{  
                removeLineStage.close();
                dataManager.removeLine(line);
            });
            cancelButton.setOnAction(e->{
                removeLineStage.close();
            });
            GridPane.setMargin(confirmation, new Insets(30,0,0,100));
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));
            GridPane.setConstraints(confirmation, 0, 1);
            GridPane.setConstraints(buttonHBox, 0, 2);
            root.getChildren().addAll(confirmation, buttonHBox);
            Scene removeLine = new Scene(root, 325, 200);
            removeLineStage.setScene(removeLine);
            removeLineStage.showAndWait();
        }
    }
    
    /**
     * Handles the response to add a station to the canvas
     */
    public void processAddStationToLineTool(){
        dataManager.setState(mmmState.ADD_STATION_MODE);
    }
    
    public void processRemoveStationFromLineTool(String currentValue){
        dataManager.setState(mmmState.REMOVE_STATION_MODE);
        dataManager.setComboLine(currentValue);
    }
    
    /**
     * Handles the request list the stations of the selected line
     */
    public void processListStationsTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        if(workspace.getMetroLineComboBox().getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No lines detected!");
            alert.setContentText("Please add some lines!");
            alert.showAndWait();
        }
        else{
            MetroLine currentLine = (MetroLine)workspace.getMetroLineComboBox().getValue();
            Stage editLineStage = new Stage();
            editLineStage.setTitle(currentLine.toString() + " Stations: ");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);
                VBox stationVBox = new VBox();
                for(int i = 0; i < currentLine.getStations().size(); i++){
                    Label station = new Label(currentLine.getStations().get(i).toString());
                    stationVBox.getChildren().add(station);
                }
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction((ActionEvent e)->{     
                   editLineStage.close();                  
                   workspace.updateEditLineButton();    //Updates LineColorButton                                    
            });
            cancelButton.setOnAction(e->{
                editLineStage.close();
            });
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));    
            GridPane.setMargin(stationVBox, new Insets(10,0,0,15));
            GridPane.setConstraints(buttonHBox, 0, 1);
            GridPane.setConstraints(stationVBox, 0, 0);
            root.getChildren().addAll(stationVBox, buttonHBox);
            Scene addLine = new Scene(root, 325, 200);
            editLineStage.setScene(addLine);
            editLineStage.showAndWait();
        }
    }
    
    /**
     * Handles the response to alter the line thickness
     */
    public void processLineThicknessTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        double originalLineThickness = workspace.getLineThickness();
	int lineThickness = (int)workspace.getLineSlider().getValue();
        if(workspace.getMetroLineComboBox().getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No lines detected!");
            alert.setContentText("Please add some lines\n"
                    + "to be edited!");
            alert.showAndWait();
        }
        else{
            MetroLine currentLine = (MetroLine)workspace.getMetroLineComboBox().getValue();
            dataManager.setCurrentLineThickness(lineThickness, originalLineThickness, currentLine);
        }
	
	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * Handles the response to color the selected station object
     */
    public void processStationColorTool(Color color){
        dataManager.setCurrentFillColor(color);
    }
    
    /**
     * Handles the request to add a station to the canvas
     */
    public void processAddStationTool(){
        //Scene scene = app.getGUI().getPrimaryScene();
        //scene.setCursor(Cursor.CROSSHAIR);  //Changes Crosshair
        
        //PROMPT
            Stage addStationStage = new Stage();
            addStationStage.setTitle("Creating a Station...");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);
                //ColorPicker
                Label colorFieldLabel = new Label("Color of Station: ");
                ColorPicker colorPicker = new ColorPicker();   
                colorPicker.setOnAction(e->{
                    elementColor = colorPicker.getValue();
                });
                HBox colorHBox = new HBox();
                colorHBox.getChildren().addAll(colorFieldLabel, colorPicker);
                //TextInput
                Label textFieldLabel = new Label("Name of Station: ");
                TextField textField = new TextField();               
                HBox textHBox = new HBox();
                textHBox.getChildren().addAll(textFieldLabel, textField);               
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction(e->{
                if(textField.getText() != null && !textField.getText().isEmpty()){
                   elementText = textField.getText();                  
                   addStationStage.close();
                   dataManager.startNewMetroStation(elementText, elementColor); 
                }
                else{
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("No text detected!");
                    alert.setContentText("Please enter a station name.");
                    alert.showAndWait();
                }
            });
            cancelButton.setOnAction(e->{
                addStationStage.close();
            });
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));    
            GridPane.setMargin(textHBox, new Insets(10,0,0,15));
            GridPane.setMargin(colorHBox, new Insets(10,0,0,15));  
            GridPane.setConstraints(buttonHBox, 0, 2);
            GridPane.setConstraints(textHBox, 0, 0);
            GridPane.setConstraints(colorHBox,0, 1);
            root.getChildren().addAll(textHBox, colorHBox, buttonHBox);
            Scene addStation = new Scene(root, 325, 200);
            addStationStage.setScene(addStation);
            addStationStage.showAndWait();
        
        //END PROMPT
        
        //dataManager.startNewMetroLabel();
        
        //RELOADS WORKSPACE
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to remove a station from the canvas
     */
    public void processRemoveStationTool(){
        ComboBox stationBox = ((mmmWorkspace)app.getWorkspaceComponent()).getMetroStationComboBox();
        
        if(dataManager.getSelectedElement() == null && !stationBox.getItems().isEmpty()){
                dataManager.setSelectedElement((Shape)stationBox.getValue());
        }
        if(dataManager.getSelectedElement() == null && stationBox.getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No selected element detected!");
            alert.setContentText("Please select a station to be removed!");
            alert.showAndWait();
        }
        else if(!(((MetroElement)dataManager.getSelectedElement()).getShapeType().equals(STATION))){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("Selected element is not a station!");
            alert.setContentText("Please select a STATION to be removed!");
            alert.showAndWait();
        }
        else if(((MetroStation)dataManager.getSelectedElement()).isLineEnd()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("Selected element is a line end!");
            alert.setContentText("Please select a STATION to be removed!");
            alert.showAndWait();
        }
        else{            
            //PROMPT
            Stage removeStationStage = new Stage();
            removeStationStage.setTitle("Remove a Station...");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);        
                //Are you sure?
                Label confirmation = new Label("Are you sure you wish to \nremove the selected station?");
                confirmation.setAlignment(Pos.CENTER);
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction(e->{  
                removeStationStage.close();
                dataManager.removeStation();
            });
            cancelButton.setOnAction(e->{
                removeStationStage.close();
            });
            GridPane.setMargin(confirmation, new Insets(30,0,0,100));
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));
            GridPane.setConstraints(confirmation, 0, 1);
            GridPane.setConstraints(buttonHBox, 0, 2);
            root.getChildren().addAll(confirmation, buttonHBox);
            Scene removeLine = new Scene(root, 325, 200);
            removeStationStage.setScene(removeLine);
            removeStationStage.showAndWait();
        
            //END PROMPT

            //dataManager.startNewMetroLabel();

            //RELOADS WORKSPACE
            mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        }
    }
    
    
    /**
     * Handles the request to snap the element to the grid
     */
    public void processSnapToGridTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();  
        ComboBox stationBox = workspace.getMetroStationComboBox();
        dataManager.snapToGrid();
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to move the selected label
     */
    public void processMoveLabelTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();     
        ComboBox stationBox = workspace.getMetroStationComboBox();
        if(stationBox.getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("There are currently no stations!");
            alert.setContentText("Please add a STATION before moving its label");
            alert.showAndWait();
        }
        else{
            dataManager.moveMetroLabel((MetroStation)stationBox.getValue());
        }
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to rotate the selected label
     */
    public void processRotateLabelTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();   
        ComboBox stationBox = ((mmmWorkspace)app.getWorkspaceComponent()).getMetroStationComboBox();
        if(stationBox.getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("There are currently no stations!");
            alert.setContentText("Please add a STATION before rotating its label");
            alert.showAndWait();
        }
        else{
            dataManager.rotateMetroLabel((MetroStation)stationBox.getValue());
        }
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to alter the station's thickness
     */
    public void processStationThicknessTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        double originalStationRadius = workspace.getStationRadius();
	int stationRadius = (int)workspace.getStationSlider().getValue();
        if(workspace.getMetroStationComboBox().getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No stations detected!");
            alert.setContentText("Please add some stations\n"
                    + "to be edited!");
            alert.showAndWait();
        }
        else{
            MetroStation currentStation = (MetroStation)workspace.getMetroStationComboBox().getValue();
            dataManager.setCurrentStationRadius(stationRadius, originalStationRadius, currentStation);
        }
	
	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * Handles the request to generate a route
     */
    public void processRouteTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        MetroStation startStation = (MetroStation)workspace.getStartStationComboBox().getValue();
        MetroStation endStation = (MetroStation)workspace.getEndStationComboBox().getValue();
        
        if(workspace.getStartStationComboBox().getItems().isEmpty() || workspace.getEndStationComboBox().getItems().isEmpty()){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("ERROR");
            alert.setHeaderText("No stations detected!");
            alert.setContentText("Please add some lines!");
            alert.showAndWait();
        }
        else{
            dataManager.bfs(startStation);
            
            Stage editLineStage = new Stage();
            editLineStage.setTitle("Shortest Path : ");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);
                VBox stationVBox = new VBox();
                
                Label station = new Label();
                if(endStation.getPath().isEmpty()){
                    station = new Label("No path found.");
                    stationVBox.getChildren().add(station);   
                }else{
                    Label start = new Label("Origin : " + startStation.toString());
                    Label end = new Label("Destination : " + endStation.toString());
                    
                    stationVBox.getChildren().addAll(start, end);
                    
                    MetroStation firstStation = null;
                    //Goes through array
                    for(int i = 0; i < endStation.getPath().size(); i++){
                        if(firstStation == null){                           
                            firstStation = endStation.getPath().get(i);
                            station = new Label("Board " + firstStation.getMasterLine().toString() + " at " + firstStation.toString());
                            stationVBox.getChildren().add(station);
                        }
                        else{
                            if(!firstStation.getMasterLine().toString().equals(endStation.getPath().get(i).getMasterLine().toString())){
                                station = new Label("Take transfer on " + firstStation.toString() + " to " + endStation.getPath().get(i).getMasterLine().toString() + ".");
                                stationVBox.getChildren().add(station);
                            }
                            station = new Label("Take the train from " + firstStation.toString() + " to " + endStation.getPath().get(i).toString() + ".");
                            stationVBox.getChildren().add(station);                       
                            firstStation = endStation.getPath().get(i);
                        }                  
                    }
                    station = new Label("Disembark " + endStation.getMasterLine().toString() + " at " + endStation.getPath().getLast().toString() + ".");
                    stationVBox.getChildren().add(station);  
                }
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction((ActionEvent e)->{     
                   editLineStage.close();                  
                   workspace.updateEditLineButton();    //Updates LineColorButton                                    
            });
            cancelButton.setOnAction(e->{
                editLineStage.close();
            });
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));    
            GridPane.setMargin(stationVBox, new Insets(10,0,0,15));
            GridPane.setConstraints(buttonHBox, 0, 1);
            GridPane.setConstraints(stationVBox, 0, 0);
            root.getChildren().addAll(stationVBox, buttonHBox);
            Scene addLine = new Scene(root, 500, 500);
            editLineStage.setScene(addLine);
            editLineStage.showAndWait();
        }       
    }
    
    /**
     * Handles the request to alter the color
     */
    public void processDecorColorTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	Color selectedColor = workspace.getMetroDecorColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.setBackgroundColor(selectedColor);
	    app.getGUI().updateToolbarControls(false);
	}
    }
    
    /**
     * Handles the request to change the background to a new iamge
     */
    public void processImageBackgroundTool(){
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
                           
        Image shapeImage = new Image("http://sample.com/res/flower.png", 100, 0, false, false);
        
        FileChooser fc = new FileChooser();
	fc.setTitle("Import an Image");
        fc.getExtensionFilters().addAll(
            new ExtensionFilter("Image Files", "*.png", "*.jpg"));
        
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        try{
            shapeImage = new Image(selectedFile.toURL().toString());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        // CHANGE THE STATE     
        dataManager.setBackgroundImage(shapeImage, selectedFile.getPath());
       
	// ENABLE/DISABLE THE PROPER BUTTONS
	mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to add an image
     */
    public void processAddImageTool(){
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
                           
        Image shapeImage = new Image("http://sample.com/res/flower.png", 100, 0, false, false);
        
        FileChooser fc = new FileChooser();
	fc.setTitle("Import an Image");
        fc.getExtensionFilters().addAll(
            new ExtensionFilter("Image Files", "*.png", "*.jpg"));
        
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        if(selectedFile != null){
            try{
            shapeImage = new Image(selectedFile.toURL().toString());
            }
            catch(Exception e){
                e.printStackTrace();
            }

            // CHANGE THE STATE     
            dataManager.addImage(shapeImage, selectedFile);
        }
       
	// ENABLE/DISABLE THE PROPER BUTTONS
	mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to add a label to the canvas
     */
    public void processAddLabelTool(){
        //Scene scene = app.getGUI().getPrimaryScene();
        //scene.setCursor(Cursor.CROSSHAIR);  //Changes Crosshair
        
        //PROMPT
            Stage addStationStage = new Stage();
            addStationStage.setTitle("Creating a Label...");
            GridPane root = new GridPane();
            root.setPadding(new Insets(0,10,0,10));
            root.setVgap(5);
            root.setHgap(2);
                //TextInput
                Label textFieldLabel = new Label("Name of Label: ");
                TextField textField = new TextField();               
                HBox textHBox = new HBox();
                textHBox.getChildren().addAll(textFieldLabel, textField);               
                //OK + Cancel Button               
                Button okButton = new Button("OK");
                Button cancelButton = new Button("Cancel");
                HBox buttonHBox = new HBox();
                buttonHBox.setSpacing(10);
                buttonHBox.getChildren().addAll(okButton, cancelButton);
            
            okButton.setOnAction(e->{
                if(textField.getText() != null && !textField.getText().isEmpty()){
                   elementText = textField.getText();                  
                   addStationStage.close();
                   dataManager.startNewMetroLabel(elementText); 
                }
                else{
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("No text detected!");
                    alert.setContentText("Please enter a label name.");
                    alert.showAndWait();
                }
            });
            cancelButton.setOnAction(e->{
                addStationStage.close();
            });
            GridPane.setMargin(buttonHBox, new Insets(30,0,0,100));    
            GridPane.setMargin(textHBox, new Insets(10,0,0,15));
            GridPane.setConstraints(buttonHBox, 0, 2);
            GridPane.setConstraints(textHBox, 0, 0);
            root.getChildren().addAll(textHBox, buttonHBox);
            Scene addStation = new Scene(root, 325, 200);
            addStationStage.setScene(addStation);
            addStationStage.showAndWait();
        
        //END PROMPT
        
        //dataManager.startNewMetroLabel();
        
        //RELOADS WORKSPACE
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to remove an element from the canvas
     */
    public void processRemoveElementTool(){
        dataManager.removeSelectedElement();
	
	// ENABLE/DISABLE THE PROPER BUTTONS
	mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * Handles the request to change the color of the font
     */
    public void processFontColorTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        dataManager.changeFontColor(workspace.getMetroFontColorPicker().getValue());
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to make the selected text bold
     */
    public void processFontBoldTool(){
        dataManager.boldText();
        app.getGUI().updateToolbarControls(false);
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to italicize the selected text
     */
    public void processFontItalics(){
        dataManager.italicizeText();
        app.getGUI().updateToolbarControls(false);
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to change the size of the font
     */
    public void processFontSize(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        
        dataManager.changeFontSize(
                Double.parseDouble(workspace.getMetroFontSizeComboBox().getSelectionModel().getSelectedItem().toString())
        );
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to change the size of the font
     */
    public void processFontStyle(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();     
        dataManager.changeFontStyle(
                workspace.getMetroFontStyleComboBox().getSelectionModel().getSelectedItem().toString()
        );
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to enable/disable the grid
     */
    public void processGridDisplayTool(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();    
        if(gridOn == false){ 
            dataManager.toggleGrid(gridOn);
            gridOn = true;
        }
        else if(gridOn == true){                   
            dataManager.toggleGrid(gridOn);
            gridOn = false;
        }
        
        
        app.getGUI().updateToolbarControls(false);       
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Handles the request to zoom in
     */
    public void processZoomInTool(){
        dataManager.mapZoom(true);
    }
    
    /**
     * Handles the request to zoom in
     */
    public void processZoomOutTool(){
        dataManager.mapZoom(false);
    }
    
    /**
     * Handles the request to increase the map size
     */
    public void processMapSizeIncreaseTool(){
        dataManager.mapSizeChange(true);
    }
    
    /**
     * Handles the request to decrease the map size.
     */
    public void processMapSizeDecreaseTool(){
        dataManager.mapSizeChange(false);
    }
    
    public void processNavigateViewPort(String direction){
        dataManager.navigateViewport(direction);
    }
    /**
     * Handles the request to display about
     */
    public void processAboutButton(){
        dataManager.about();
    }
    
    public void processComboLineChange(String newValue){
        dataManager.setComboLine(newValue);
    }
    
    public void processComboStationChange(String newValue){
        dataManager.setComboStation(newValue);
        dataManager.setSelectedElement(dataManager.getComboStationObject());
    }
    
    public void processUndoButton(){
        dataManager.undo();
    }
    
    public void processRedoButton(){
        dataManager.redo();
    }
    
    public void processExportButton(){
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
	File file = new File(PATH_WORK + dataManager.getFileName() + "/" +  dataManager.getFileName() + " Metro" + ".png");
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            app.getFileComponent().exportData(dataManager, PATH_WORK + 
                    dataManager.getFileName() + "/" + 
                    dataManager.getFileName() + " Metro" +".json");
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	}
        
        
    }
}
