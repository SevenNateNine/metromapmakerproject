/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import mmm.data.MetroElement;
import static mmm.data.MetroElement.LABEL;
import static mmm.data.MetroElement.STATION;
import mmm.data.MetroLabel;
import mmm.data.MetroStation;
import mmm.data.mmmData;
import mmm.data.mmmState;
import static mmm.data.mmmState.*;
import mmm.transactions.MoveElement;

/**
 * Handles mouse events on the canvas
 * @author natec
 */
public class CanvasController {
    AppTemplate app;
    MetroElement startShape;
    MetroElement endShape;
    double startX;
    double startY;
    double endX;
    double endY;
    
    /**
     * Constructor of CanvasController class
     * @param initApp 
     *      Used to set the app variable
     */
    public CanvasController(AppTemplate initApp){
        app = initApp;
    }
    
    /**
     * Performs an action when the mouse is pressed on the canvas
     * @param x
     *      The x coordinate of the cursor
     * @param y 
     *      The y coordinate of the cursor
     */
    public void processCanvasMousePress(int x, int y){
        
        mmmData dataManager = (mmmData) app.getDataComponent();
        if(dataManager.isInState(SELECTING_ELEMENT)){
            //SELECT TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            
            Scene scene = app.getGUI().getPrimaryScene();
            
            //DRAGGING MY DUDE
            if(shape != null && !(shape instanceof Line)){
                if(((MetroElement)shape).getShapeType().equals(LABEL)){
                    if(((MetroLabel)shape).isEnd()){
                        //((MetroLabel)shape).getMasterStation().setStroke(Color.YELLOW);
                        ((MetroLabel)shape).setVisible(false);
                        ((MetroLabel)shape).getMasterStation().setFill(Color.SALMON);
                        
                    }
                    else{
                        scene.setCursor(Cursor.MOVE);
                        startShape = (MetroElement)shape;
                        dataManager.setState(mmmState.DRAGGING_ELEMENT);                        
                        app.getGUI().updateToolbarControls(false);
                    }
                    ((mmmWorkspace)app.getWorkspaceComponent()).updateFontItems(((MetroLabel)shape));
                    //Enable Font Items as well
                }
                else{
                    scene.setCursor(Cursor.MOVE);
                    startShape = (MetroElement)shape;
                    dataManager.setState(mmmState.DRAGGING_ELEMENT);
                    app.getGUI().updateToolbarControls(false);
                }
                startX = startShape.getX();
                startY = startShape.getY();
            }
            else{
                dataManager.deselectStations();
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        }
        else if(dataManager.isInState(ADD_STATION_MODE)){
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);
            if(shape != null){
                if(((MetroElement)shape).getShapeType().equals(STATION)){
                    MetroStation selectedStation = (MetroStation)shape;
                    if(!dataManager.getComboLineObject().getStations().contains(selectedStation)){
                        dataManager.addStationToLine((MetroStation)shape);
                    }
                }
            }
            else{
                dataManager.setState(SELECTING_ELEMENT);
            }
        }
        else if(dataManager.isInState(REMOVE_STATION_MODE)){
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);
            if(shape != null){
                if(shape instanceof MetroStation){
                    if(((MetroStation)shape).inLine()){
                        dataManager.removeStationFromLine((MetroStation)shape);
                    }
                }
            }
            else{
                dataManager.setState(SELECTING_ELEMENT);
            }
        }
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * Performs an action when the mouse is dragged across the canvas.
     * @param x
     *      The x coordinate of the cursor
     * @param y 
     *      The y coordinate of the cursor
     */
    public void processCanvasMouseDragged(int x, int y){
        mmmData dataManager = (mmmData)app.getDataComponent();
        if(dataManager.isInState(DRAGGING_ELEMENT)){
            MetroElement selectedDraggableShape = (MetroElement)dataManager.getSelectedElement();
            if(selectedDraggableShape instanceof MetroLabel){
                if(((MetroLabel)selectedDraggableShape).getMasterStation() == null){
                    selectedDraggableShape.drag(x, y);
                }
            }
            else{
                selectedDraggableShape.drag(x, y);
            }
            endX = x;
            endY = y;
            
            app.getGUI().updateToolbarControls(false);
        }
    }
    
    /**
     * Performs an action when the mouse is released
     * @param x
     *      The x coordinate of the cursor 
     * @param y 
     *      The y coordinate of the cursor
     */
    public void processCanvasMouseRelease(int x, int y){
        mmmData dataManager = (mmmData) app.getDataComponent();
        if(dataManager.isInState(mmmState.DRAGGING_ELEMENT)){
            dataManager.setState(SELECTING_ELEMENT);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            endShape = (MetroElement)dataManager.getSelectedElement();
            
            jTPS_Transaction transaction = 
                    new MoveElement(endShape, startX, startY, endX, endY);
            dataManager.getjTPS().addTransaction(transaction);
            
            app.getGUI().updateToolbarControls(false);
        }
        else if(dataManager.isInState(mmmState.DRAGGING_NOTHING)){
            dataManager.setState(SELECTING_ELEMENT);
        }
    }
}
