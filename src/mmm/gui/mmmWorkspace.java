package mmm.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.NEW_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.NEW_COMPLETED_TITLE;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import static mmm.css.mmmStyle.*;
import mmm.data.MetroElement;
import static mmm.data.MetroElement.LINE;
import static mmm.data.MetroElement.STATION;
import mmm.data.MetroLabel;
import mmm.data.MetroLine;
import mmm.data.MetroLineSegment;
import mmm.data.MetroStation;
import mmm.data.mmmData;
import static mmm.data.mmmData.BLACK_HEX;
import static mmm.data.mmmData.WHITE_HEX;
import mmm.data.mmmState;
import static mmm.mmmLanguageProperty.*;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class mmmWorkspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    //New Buttons
    VBox editToolbar;
    VBox metroLineToolbar;
    
    HBox metroLineToolbarPart1;
    Label metroLineLabel;
    ComboBox<MetroLine> metroLineComboBox;
    StackPane metroLineEditLineButton;
        Circle editLine;
        Text editLineText;
    
    HBox metroLineToolbarPart2;
    Button addLineButton;
    Button removeLineButton;
    Button addStationToLineButton;
    Button removeStationFromLineButton;
    Button listStationsButton;
    
    Slider metroLineThickness;
    
    VBox metroStationToolbar;
    HBox metroStationToolbarPart1;
    Label metroStationsLabel;
    ComboBox<MetroStation> metroStationComboBox;
    ColorPicker metroStationColorButton;
    HBox metroStationToolbarPart2;
    Button addStationButton;
    Button removeStationButton;
    Button snapToGridButton;
    Button moveLabelButton;
    Button rotateLabelButton;
    Slider metroStationThickness;
    
    HBox metroRouteToolbar;
    VBox metroRouteToolbarPart1;
    ComboBox<MetroStation> startDestinationComboBox;
    ComboBox<MetroStation> endDestinationComboBox;
    Button findRouteButton;
    
    VBox metroDecorToolbar;
    HBox metroDecorToolbarPart1;
    Label metroDecorLabel;
    ColorPicker metroDecorColorButton;
    HBox metroDecorToolbarPart2;
    Button setImageBackgroundButton;
    Button addImageButton;
    Button addLabelButton;
    Button removeElementButton;
    
    VBox metroFontToolbar;
    HBox metroFontToolbarPart1;
    Label metroFontLabel;
    ColorPicker metroFontColorButton;
    HBox metroFontToolbarPart2;
    Button metroBoldButton;
    Button metroItalicsButton;
    ComboBox<String> metroFontSizeComboBox;
    ComboBox<String> metroFontStyleComboBox;
    VBox metroNavigationToolbar;
    HBox metroNavigationToolbarPart1;
    Label metroNavigationLabel;
    CheckBox metroNavigationCheckBox;
    HBox metroNavigationToolbarPart2;
    Button metroZoomInButton;
    Button metroZoomOutButton;
    Button metroMapSizeIncreaseButton;
    Button metroMapSizeDecreaseButton;
    
    //UNDO, REDO, ABOUT
    Button undoButton, redoButton, aboutButton, exportButton;
        
    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;
    Pane imageCanvas;
    Pane backgroundCanvas;
    StackPane pairingCanvas;
    
    ScrollPane parentCanvas;
    BorderPane masterCanvas;
    
    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    MapEditController mapEditController;    

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;

    //PART 9
    double lineThickness;
    double stationRadius;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface
	gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
        .
     */
    public mmmWorkspace(AppTemplate initApp) {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
        
        //welcomeDialog();
    }
    
    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    

    public Pane getCanvas() {
	return canvas;
    }
    
    public BorderPane getMasterCanvas(){
        return masterCanvas;
    }
        
    public ScrollPane getParentCanvas(){
        return parentCanvas;
    }
    
    public Pane getBackgroundCanvas(){
        return backgroundCanvas;
    }
    
    public Pane getImageCanvas(){
        return imageCanvas;
    }
    
    // HELPER SETUP METHOD
    private void initLayout() {
        
        
	// THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
	editToolbar = new VBox();
        
        //New Buttons
        metroLineToolbar = new VBox();
        
        //METRO LINES
        metroLineToolbarPart1 = new HBox();
        metroLineToolbarPart1.setSpacing(20);
        //PART 1
        metroLineLabel = new Label("Metro Lines");
        metroLineComboBox = new ComboBox<MetroLine>();
            
        metroLineEditLineButton = new StackPane();
            editLine = new Circle();
            editLine.setRadius(30);
            editLine.setFill(Color.WHITE);
            editLineText = new Text(Color.WHITE.toString());
            editLineText.setBoundsType(TextBoundsType.VISUAL); 
            editLineText.setFont(Font.font("Monospaced", 10));
            metroLineEditLineButton.getChildren().addAll(editLine, editLineText);
        metroLineToolbarPart1.getChildren().addAll(
                metroLineLabel,
                metroLineComboBox,
                metroLineEditLineButton
        );
        //PART 2
        metroLineToolbarPart2 = new HBox();
        //metroLineToolbarPart2.setSpacing(20);
        addLineButton = gui.initChildButton(metroLineToolbarPart2, ADDLINE_ICON.toString(), ADDLINE_TOOLTIP.toString(), false);
        removeLineButton = gui.initChildButton(metroLineToolbarPart2, REMOVELINE_ICON.toString(), REMOVELINE_TOOLTIP.toString(), false);
        addStationToLineButton = this.createTextButton(metroLineToolbarPart2, "Add\nStation", ADDSTATION_TOLINE_TOOLTIP.toString(), false);
        removeStationFromLineButton = this.createTextButton(metroLineToolbarPart2, "Remove\nStation", REMOVESTATION_FROMLINE_TOOLTIP.toString(), false);
        listStationsButton = gui.initChildButton(metroLineToolbarPart2, LISTSTATIONS_ICON.toString(), LISTSTATIONS_TOOLTIP.toString(), false);
               
        metroLineThickness = new Slider(0, 10, 1);  //Add last
        metroLineThickness.setMin(5);
        metroLineThickness.setMax(20);
                
        metroLineToolbar.getChildren().addAll(
                metroLineToolbarPart1,
                metroLineToolbarPart2,
                metroLineThickness
        );
        
        //METRO STATIONS
        metroStationToolbar = new VBox();
        metroStationToolbarPart1 = new HBox();
        metroStationsLabel = new Label("Metro Stations");
        ObservableList<MetroStation> metroStations = FXCollections.observableArrayList();
        metroStationComboBox = new ComboBox<MetroStation>(metroStations);
        metroStationColorButton = new ColorPicker();       
        metroStationToolbarPart1.getChildren().addAll(
                metroStationsLabel,
                metroStationComboBox,
                metroStationColorButton
        );
        //PART 2 
        metroStationToolbarPart2 = new HBox();
        addStationButton = gui.initChildButton(metroStationToolbarPart2, ADDSTATION_ICON.toString(), ADDSTATION_TOOLTIP.toString(), false);
        removeStationButton = gui.initChildButton(metroStationToolbarPart2, REMOVESTATION_ICON.toString(), REMOVESTATION_TOOLTIP.toString(), false);
        snapToGridButton = this.createTextButton(metroStationToolbarPart2, "Snap", SNAPTOGRID_TOOLTIP.toString(), false);
        moveLabelButton = this.createTextButton(metroStationToolbarPart2, "Move\nLine", MOVELABEL_TOOLTIP.toString(), false);
        rotateLabelButton = gui.initChildButton(metroStationToolbarPart2, ROTATELABEL_ICON.toString(), ROTATELABEL_TOOLTIP.toString() ,false);
        
        metroStationThickness = new Slider(0, 10, 1);  //Add last
        metroStationThickness.setMin(10);
        metroStationThickness.setMax(40);
        
        metroStationToolbar.getChildren().addAll(
                metroStationToolbarPart1,
                metroStationToolbarPart2,
                metroStationThickness
        );
        
        metroRouteToolbar = new HBox();
        metroRouteToolbarPart1 = new VBox();
        startDestinationComboBox = new ComboBox<MetroStation>(metroStations);
        endDestinationComboBox = new ComboBox<MetroStation>(metroStations);
        metroRouteToolbarPart1.getChildren().addAll(
                startDestinationComboBox,
                endDestinationComboBox
        );
        
        metroRouteToolbar.getChildren().add(metroRouteToolbarPart1);
        findRouteButton = gui.initChildButton(metroRouteToolbar, FINDROUTE_ICON.toString(), FINDROUTE_TOOLTIP.toString(), false);
               
        //DECOR
        metroDecorToolbar = new VBox();
        metroDecorToolbarPart1 = new HBox();
        metroDecorToolbarPart1.setSpacing(30);
        metroDecorLabel = new Label("Decor");
        metroDecorColorButton = new ColorPicker();
        metroDecorToolbarPart1.getChildren().addAll(
                metroDecorLabel, 
                metroDecorColorButton);        
        
        metroDecorToolbarPart2 = new HBox();
        setImageBackgroundButton = this.createTextButton(metroDecorToolbarPart2, "Set Image\nBackground", IMAGEBACKGROUND_TOOLTIP.toString(), false);
        addImageButton = this.createTextButton(metroDecorToolbarPart2, "Add\nImage", ADDIMAGE_TOOLTIP.toString(), false);
        addLabelButton = this.createTextButton(metroDecorToolbarPart2, "Add\nLabel", ADDLABEL_TOOLTIP.toString(), false);
        removeElementButton = this.createTextButton(metroDecorToolbarPart2, "Remove\nElement", REMOVEELEMENT_TOOLTIP.toString(), false);
        metroDecorToolbar.getChildren().addAll(
                metroDecorToolbarPart1, 
                metroDecorToolbarPart2
        );
        
        //FONT
        ObservableList<String> fontSizeOptions = 
            FXCollections.observableArrayList(
                "8", "10", "12", "14", "16", "18", "20", "22", "24", "26",
                "28", "30", "32", "34", "36", "38", "40", "42", "44", "46"          
                    );
        ObservableList<String> fontStyleOptions = 
        FXCollections.observableArrayList(
            "Serif",
            "SansSerif",
            "Monospaced",
            "Verdana",
            "Dialog"
                );
        metroFontToolbar = new VBox();
        metroFontToolbarPart1 = new HBox();
        metroFontLabel = new Label("Font");
        metroFontColorButton = new ColorPicker();
        metroFontToolbarPart1.getChildren().addAll(metroFontLabel, metroFontColorButton);
        metroFontToolbarPart2 = new HBox();
        metroBoldButton = gui.initChildButton(metroFontToolbarPart2, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), false);
        metroItalicsButton = gui.initChildButton(metroFontToolbarPart2, ITALICS_ICON.toString(), ITALICS_TOOLTIP.toString(), false);
        metroFontSizeComboBox = new ComboBox<String>(fontSizeOptions);
        metroFontStyleComboBox = new ComboBox<String>(fontStyleOptions);
        metroFontToolbarPart2.getChildren().addAll(
                metroFontSizeComboBox, 
                metroFontStyleComboBox
        );
        
        metroFontToolbar.getChildren().addAll(
                metroFontToolbarPart1,
                metroFontToolbarPart2
        );
        
        //NAVIGATION
        metroNavigationToolbar = new VBox();
        metroNavigationToolbarPart1 = new HBox();
        metroNavigationLabel = new Label ("Navigation");
        metroNavigationCheckBox = new CheckBox("Show Grid");
        metroNavigationToolbarPart1.setSpacing(50);
        metroNavigationToolbarPart1.getChildren().addAll(
                metroNavigationLabel,
                metroNavigationCheckBox
        );
        
        metroNavigationToolbarPart2 = new HBox();
        metroZoomInButton = gui.initChildButton(metroNavigationToolbarPart2, ZOOMIN_ICON.toString(), ZOOMIN_TOOLTIP.toString(), false);
        metroZoomOutButton = gui.initChildButton(metroNavigationToolbarPart2, ZOOMOUT_ICON.toString(), ZOOMOUT_TOOLTIP.toString(), false);
        metroMapSizeIncreaseButton = gui.initChildButton(metroNavigationToolbarPart2, MAPINCREASE_ICON.toString(), MAPINCREASE_TOOLTIP.toString(), false);
        metroMapSizeDecreaseButton = gui.initChildButton(metroNavigationToolbarPart2, MAPDECREASE_ICON.toString(), MAPDECREASE_TOOLTIP.toString(), false);
        
        metroNavigationToolbar.getChildren().addAll(
                metroNavigationToolbarPart1, 
                metroNavigationToolbarPart2
        );
        
        editToolbar.getChildren().addAll(
                metroLineToolbar, 
                metroStationToolbar, 
                metroRouteToolbar,
                metroDecorToolbar,
                metroFontToolbar,
                metroNavigationToolbar
        );
        
        //HIGHEST BUTTONS
        
        exportButton = gui.initChildButton(gui.getFileToolbar(), EXPORT_ICON.toString(), EXPORT_TOOLTIP.toString(), false);
        undoButton = gui.initChildButton(gui.getFileToolbar(), UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), false);
        redoButton = gui.initChildButton(gui.getFileToolbar(), REDO_ICON.toString(), REDO_TOOLTIP.toString(), false);
        aboutButton = gui.initChildButton(gui.getFileToolbar(), ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
        
	
	// WE'LL RENDER OUR STUFF HERE IN THE CANVAS
            //WIDTH AND HEIGHT PANES ARE TO HELP WITH ZOOMING
        masterCanvas = new BorderPane();
        Pane width = new Pane();    
        Pane height = new Pane();
        Paint borderColor = Color.SLATEGREY;
        height.setBackground(new Background(new BackgroundFill(borderColor, null, null)));
        width.setBackground(new Background(new BackgroundFill(borderColor, null, null)));
        masterCanvas.setPrefSize(1400, 880);
        masterCanvas.setMinSize(1400, 880);
        
        canvas = new Pane();
        canvas.setPrefSize(1400, 1400);
        height.setPrefSize(100, 2000);
        width.setPrefSize(2000,100);
        canvas.setMinSize(959, 959);
        canvas.setMaxSize(1400, 1400);	
        width.setMaxSize(2000, 600);
        height.setMaxSize(800, 2000);
        
        backgroundCanvas = new Pane();
        backgroundCanvas.setPrefSize(800, 800);
        
        imageCanvas = new Pane();
        imageCanvas.setPrefSize(1400, 800);
        
        //BINDING???
        //backgroundCanvas.translateXProperty().bind(canvas.widthProperty());
        //backgroundCanvas.translateYProperty().bind(canvas.heightProperty());
        //imageCanvas.translateXProperty().bind(canvas.widthProperty());
        //imageCanvas.translateYProperty().bind(canvas.heightProperty());
        
        pairingCanvas = new StackPane();
        pairingCanvas.getChildren().add(backgroundCanvas);
        pairingCanvas.getChildren().add(imageCanvas);
        pairingCanvas.getChildren().add(canvas);
        
        parentCanvas = new ScrollPane(pairingCanvas);
        
        parentCanvas.setPrefViewportWidth(1400);
        parentCanvas.setPrefViewportHeight(1000);
        parentCanvas.setPrefSize(800,800);
        parentCanvas.setMinSize(300, 300);
        parentCanvas.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        parentCanvas.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        masterCanvas.setCenter(parentCanvas);
        masterCanvas.setRight(height);
        masterCanvas.setBottom(width);

        System.out.println(parentCanvas.getHeight() + " " + parentCanvas.getWidth());
        
	debugText = new Text();
	canvas.getChildren().add(debugText);
	debugText.setX(100);
	debugText.setY(100);
	
	// AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
	mmmData data = (mmmData)app.getDataComponent();
	data.setShapes(canvas.getChildren());

	// AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(editToolbar);
	((BorderPane)workspace).setCenter(masterCanvas);
        
        workspace.setBackground(new Background(new BackgroundFill(Color.BISQUE, null, null)));
    }
    
    // HELPER SETUP METHOD
    private void initControllers() {
	// MAKE THE EDIT CONTROLLER
        //mmmData data = (mmmData)app.getDataComponent();
	mapEditController = new MapEditController(app);
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        addLineButton.setOnAction(e->{
            mapEditController.processAddLineTool();
        });
        removeLineButton.setOnAction(e->{
            if(metroLineComboBox.getValue() != null){
                mapEditController.processRemoveLineTool(metroLineComboBox.getValue());
            }                  
        });
        //Edit Line?
        //Move Line End
        addStationToLineButton.setOnAction(e->{
            mapEditController.processAddStationToLineTool();
        });
        removeStationFromLineButton.setOnAction(e->{
            mapEditController.processRemoveStationFromLineTool(metroLineComboBox.getValue().toString());
        });
        
        
        metroLineComboBox.setOnAction(e->{
            if(metroLineComboBox.getValue() != null){
                mapEditController.processComboLineChange(metroLineComboBox.getValue().toString());
                this.updateEditLineButton();
            }
        });
        
        editLine.setOnMousePressed(e->{
            if(metroLineComboBox.getValue() != null){
                mapEditController.processEditLineTool(metroLineComboBox.getValue());               
                this.updateEditLineButton();
            }
        });
        
        metroLineThickness.setOnMouseClicked(e->{
            lineThickness = metroLineThickness.getValue();
        });
        metroLineThickness.valueProperty().addListener(e->{
            mapEditController.processLineThicknessTool();
        });
        listStationsButton.setOnAction(e->{
            mapEditController.processListStationsTool();
        });
        
        //METROSTATION
        addStationButton.setOnAction(e->{
            mapEditController.processAddStationTool();
            //this.addStationHelper();
        });
        removeStationButton.setOnAction(e->{
            mapEditController.processRemoveStationTool();
        });
        
        metroStationComboBox.setOnAction(e->{
            if(metroStationComboBox.getValue() != null){
                this.updateStationColorPicker();
            }
        });
        
        metroStationColorButton.setOnAction(e->{
            mapEditController.processStationColorTool(metroStationColorButton.getValue());
        });
        metroStationThickness.setOnMouseClicked(e->{
            stationRadius = metroStationThickness.getValue();
        });
        metroStationThickness.valueProperty().addListener(e->{
            mapEditController.processStationThicknessTool();
        });
        snapToGridButton.setOnAction(e->{
            mapEditController.processSnapToGridTool();
        });
        moveLabelButton.setOnAction(e->{
            mapEditController.processMoveLabelTool();
        });
        rotateLabelButton.setOnAction(e->{
            mapEditController.processRotateLabelTool();
        });
        
        findRouteButton.setOnAction(e->{
            mapEditController.processRouteTool();
        });
        
        metroDecorColorButton.setOnAction(e->{
            mapEditController.processDecorColorTool();
        });
        addImageButton.setOnAction(e->{
            mapEditController.processAddImageTool();
        });
        setImageBackgroundButton.setOnAction(e->{
            mapEditController.processImageBackgroundTool();
        });
        addLabelButton.setOnAction(e->{
            mapEditController.processAddLabelTool();
        });
        removeElementButton.setOnAction(e->{
            mapEditController.processRemoveElementTool();
        });
        
        metroFontColorButton.setOnAction(e->{
            mapEditController.processFontColorTool();
        });
        metroBoldButton.setOnAction(e->{
            mapEditController.processFontBoldTool();
        });
        metroItalicsButton.setOnAction(e->{
            mapEditController.processFontItalics();
        });
        metroFontSizeComboBox.setOnAction(e->{
            mapEditController.processFontSize();
        });
        metroFontStyleComboBox.setOnAction(e->{
            mapEditController.processFontStyle();
        });
        
        metroZoomInButton.setOnAction(e->{
            mapEditController.processZoomInTool();
            
        });
        metroZoomOutButton.setOnAction(e->{
            mapEditController.processZoomOutTool();
        });
        metroMapSizeIncreaseButton.setOnAction(e->{
            mapEditController.processMapSizeIncreaseTool();
        });
        metroMapSizeDecreaseButton.setOnAction(e->{
            mapEditController.processMapSizeDecreaseTool();
        });
        metroNavigationCheckBox.setOnAction(e->{
            mapEditController.processGridDisplayTool();
        });
        
        workspace.setOnKeyPressed(e->{
            String direction = "";
            switch(e.getCode()){
                case W:
                    direction = "UP";
                    break;
                case S:
                    direction = "DOWN";
                    break;
                case A:
                    direction = "LEFT";
                    break;
                case D:
                    direction = "RIGHT";  
                    break;
            }
            mapEditController.processNavigateViewPort(direction);
        });
        
        aboutButton.setOnAction(e->{
            mapEditController.processAboutButton();
        });
	
        undoButton.setOnAction(e->{
            mapEditController.processUndoButton();
        });
        
        redoButton.setOnAction(e->{
            mapEditController.processRedoButton();
        });
        
        //SAVING, LOADING, EXPORTING
        exportButton.setOnAction(e->{
            mapEditController.processExportButton();
        });
        
        
        
	// MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
	canvas.setOnMousePressed(e->{
	    canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	});
        
        
        
        /*
        canvas.setOnMouseClicked(e->{
            if(e.getClickCount() == 2){
                canvasController.processCanvasMouseDoubleClick((int)e.getX(), (int)e.getY());
            }
        });
        */
        exportButton.setOnAction(e->{
            mapEditController.processExportButton();
        });
                }

    // HELPER METHOD
    public void loadSelectedShapeSettings(Shape shape) {
        if (shape != null) {
	    
            
            /*
            if(((Draggable)shape).getShapeType().equals(IMAGE)){
                fillColorPicker.setValue(null);
                outlineColorPicker.setValue(null);
                outlineThicknessSlider.setValue(0);
                fontCombo.setValue(null);
                textSizeCombo.setValue(null);
            }   
            else if(((Draggable)shape).getShapeType().equals(TEXT)){
                fillColorPicker.setValue((Color)shape.getFill());
                outlineColorPicker.setValue((Color)shape.getStroke());
                outlineThicknessSlider.setValue((double)shape.getStrokeWidth());
                fontCombo.setValue(((DraggableText)shape).getFontStyle());
                textSizeCombo.setValue(Double.toString((((DraggableText)shape).getFontSize())));
            }
            else{   //Does original function
                Color fillColor = (Color)shape.getFill();
                Color strokeColor = (Color)shape.getStroke();
                double lineThickness = shape.getStrokeWidth();
                fillColorPicker.setValue(fillColor);
                outlineColorPicker.setValue(strokeColor);
                outlineThicknessSlider.setValue(lineThickness);
                fontCombo.setValue(null);
                textSizeCombo.setValue(null);
            }
            */
	}
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
	canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
	
	// COLOR PICKER STYLE
        //metroLineColorButton.getStyleClass().add(CLASS_BUTTON);
	metroLineComboBox.getStyleClass().add(CLASS_COMBO_BOX);
        metroLineComboBox.getStyleClass().add(CLASS_COMBO_BOX_POPUP);
	editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        
        //NEWER ROWS
        metroLineToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroStationToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroRouteToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroDecorToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroFontToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroNavigationToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        
    }

    /**
     * This function reloads all the controls for editing logos
     * the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
	mmmData dataManager = (mmmData)data;       

        
        
	/*
        if (dataManager.isInState(mmmState.STARTING_RECTANGLE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(true);
	    ellipseButton.setDisable(false);                      
	}
	else if (dataManager.isInState(mmmState.STARTING_ELLIPSE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(true);
	}
	else if (dataManager.isInState(mmmState.SELECTING_SHAPE) 
		|| dataManager.isInState(mmmState.DRAGGING_SHAPE)
		|| dataManager.isInState(mmmState.DRAGGING_NOTHING)) {
            //if(((Draggable)dataManager.getSelectedShape()).getShapeType().equals(TEXT)){
                
            //}
	    boolean shapeIsNotSelected = dataManager.getSelectedShape() == null;
	    selectionToolButton.setDisable(true);
	    removeButton.setDisable(shapeIsNotSelected);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(false);
	    moveToFrontButton.setDisable(shapeIsNotSelected);
	    moveToBackButton.setDisable(shapeIsNotSelected);
            if(dataManager.getSelectedShape()!=null){
                if(((Draggable)dataManager.getSelectedShape()).getShapeType().equals(TEXT)){
                    fontCombo.setDisable(false);
                    textSizeCombo.setDisable(false);
                    boldButton.setDisable(false);
                    italicButton.setDisable(false);
                }
                else{
                    fontCombo.setDisable(true);
                    textSizeCombo.setDisable(true);
                    boldButton.setDisable(true);
                    italicButton.setDisable(true);
                }
            }
            
            HBox textEditorBox;
    ComboBox<String> fontCombo;
    ComboBox<String> textSizeCombo;
    Button boldButton;
    Button italicButton;
            
	}
	
	removeButton.setDisable(dataManager.getSelectedShape() == null);
	backgroundColorPicker.setValue(dataManager.getBackgroundColor());
        */
    }
    
    @Override
    public void resetWorkspace() {
        // WE ARE NOT USING THIS, THOUGH YOU MAY IF YOU LIKE
    }
    
    
    public void addLineHelper(MetroLine newLine){  
        metroLineComboBox.setCellFactory(
                new Callback<ListView<MetroLine>, ListCell<MetroLine>>(){
                    @Override
                    public ListCell<MetroLine> call (ListView<MetroLine> line){
                        ListCell<MetroLine> cell = new ListCell<MetroLine>(){
                        @Override
                        protected void updateItem(MetroLine line, boolean empty){
                            super.updateItem(line, empty);
                            if(empty){
                                setText("");
                            }
                            else{
                                setText(line.toString());
                            }
                        }
                    };
                    return cell;
                }
        });
        metroLineComboBox.setButtonCell(
                new ListCell<MetroLine>(){
                    @Override
                    protected void updateItem(MetroLine line, boolean bln){
                        super.updateItem(line, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(line.toString());
                        }
                    }
                }
        );
        metroLineComboBox.getItems().add(newLine);
        metroLineComboBox.setValue(newLine);
        this.updateEditLineButton();
    }
    
    /**
     * Checks if lines have been removed and then removes them from the comboBox.
     */
    public void removeLineHelper(MetroLine line){
        metroLineComboBox.getItems().remove(line);
        if(!metroLineComboBox.getItems().isEmpty()){
            metroLineComboBox.setValue(
                                metroLineComboBox.getItems().get(
                                        metroLineComboBox.getItems().size()-1
                                )
                        );
        }
        else{
            metroLineComboBox.setValue(null);
        }
        this.updateEditLineButton();
    }
    
    public void addStationHelper(MetroStation newStation){
        metroStationComboBox.setCellFactory(
                new Callback<ListView<MetroStation>, ListCell<MetroStation>>(){
                    @Override
                    public ListCell<MetroStation> call (ListView<MetroStation> s){
                        ListCell<MetroStation> cell = new ListCell<MetroStation>(){
                        @Override
                        protected void updateItem(MetroStation station, boolean empty){
                            super.updateItem(station, empty);
                            if(empty){
                                setText("");
                            }
                            else{
                                setText(station.toString());
                            }
                        }
                    };
                    return cell;
                }
        });
        metroStationComboBox.setButtonCell(
                new ListCell<MetroStation>(){
                    @Override
                    protected void updateItem(MetroStation s, boolean bln){
                        super.updateItem(s, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(s.toString());
                        }
                    }
                }
        );
        metroStationComboBox.getItems().add(newStation);
        metroStationComboBox.setValue(newStation);
        //START DESTINATION COMBO BOX
        startDestinationComboBox.setCellFactory(
                new Callback<ListView<MetroStation>, ListCell<MetroStation>>(){
                    @Override
                    public ListCell<MetroStation> call (ListView<MetroStation> s){
                        ListCell<MetroStation> cell = new ListCell<MetroStation>(){
                        @Override
                        protected void updateItem(MetroStation station, boolean empty){
                            super.updateItem(station, empty);
                            if(empty){
                                setText("");
                            }
                            else{
                                setText(station.toString());
                            }
                        }
                    };
                    return cell;
                }
        });
        startDestinationComboBox.setButtonCell(
                new ListCell<MetroStation>(){
                    @Override
                    protected void updateItem(MetroStation s, boolean bln){
                        super.updateItem(s, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(s.toString());
                        }
                    }
                }
        );
        //END DESTINATION COMBO BOX
        endDestinationComboBox.setCellFactory(
                new Callback<ListView<MetroStation>, ListCell<MetroStation>>(){
                    @Override
                    public ListCell<MetroStation> call (ListView<MetroStation> s){
                        ListCell<MetroStation> cell = new ListCell<MetroStation>(){
                        @Override
                        protected void updateItem(MetroStation station, boolean empty){
                            super.updateItem(station, empty);
                            if(empty){
                                setText("");
                            }
                            else{
                                setText(station.toString());
                            }
                        }
                    };
                    return cell;
                }
        });
        endDestinationComboBox.setButtonCell(
                new ListCell<MetroStation>(){
                    @Override
                    protected void updateItem(MetroStation s, boolean bln){
                        super.updateItem(s, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(s.toString());
                        }
                    }
                }
        );
        this.updateStationColorPicker();
    }
    
    public void removeStationHelper(MetroStation station){
        metroStationComboBox.getItems().remove(station);
        startDestinationComboBox.getItems().remove(station);
        endDestinationComboBox.getItems().remove(station);
        if(!metroStationComboBox.getItems().isEmpty()){
            metroStationComboBox.setValue(
                                metroStationComboBox.getItems().get(
                                        metroStationComboBox.getItems().size()-1
                                )
                        );
            startDestinationComboBox.setValue(
                                startDestinationComboBox.getItems().get(
                                        startDestinationComboBox.getItems().size()-1
                                )
                        );
           endDestinationComboBox.setValue(
                                endDestinationComboBox.getItems().get(
                                        endDestinationComboBox.getItems().size()-1
                                )
                        );
        }
        else{
            metroStationComboBox.setValue(null);
            startDestinationComboBox.setValue(null);
            endDestinationComboBox.setValue(null);
        }
        this.updateStationColorPicker();        
    }
    
    public void editLineHelper(MetroLine newLine, MetroLine oldLine){
        metroLineComboBox.getItems().remove(oldLine);
        metroLineComboBox.getItems().add(newLine);        
        metroLineComboBox.setValue(newLine);
    }
    
    public Slider getLineSlider(){
        return metroLineThickness;
    }
    
    public Slider getStationSlider(){
        return metroStationThickness;
    }
    
    public double getLineThickness(){
        return lineThickness;
    }
    
    public double getStationRadius(){
        return stationRadius;
    }
    
    private Button createTextButton(Pane toolbar, String text, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setText(text);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);	
       
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    /**
     * Updates the EditLineButton to match the color/color HEX# of the line in
     * the comboBox.
     * @param currentValue 
     */
    public void updateEditLineButton(){
        MetroLine comboLine = metroLineComboBox.getValue();
        if(!metroLineComboBox.getItems().isEmpty()){
            editLine.setFill(comboLine.getLineColor());
            editLineText.setText(comboLine.getLineColor().toString());
        }
        else{
            editLine.setFill(Color.WHITE);
            editLineText.setText(Color.WHITE.toString());
        }
    }
    
    public void updateStationColorPicker(){
        MetroStation comboStation = metroStationComboBox.getValue();
        if(!metroStationComboBox.getItems().isEmpty()){
            metroStationColorButton.setValue(comboStation.getColor());
        }
        else{
            metroStationColorButton.setValue(Color.WHITE);
        }
    }
    
    public ComboBox getMetroLineComboBox(){
        return metroLineComboBox;
    }
    
    public ComboBox getMetroStationComboBox(){
        return metroStationComboBox;
    }
    
    public ComboBox getStartStationComboBox(){
        return startDestinationComboBox;
    }
    
    public ComboBox getEndStationComboBox(){
        return endDestinationComboBox;
    }
    
    public ColorPicker getMetroFontColorPicker(){
        return metroFontColorButton;
    }
    
    public ComboBox getMetroFontStyleComboBox(){
        return metroFontStyleComboBox;
    }
    
    public ComboBox getMetroFontSizeComboBox(){
        return metroFontSizeComboBox;
    }
    
    public ColorPicker getMetroDecorColorPicker(){
        return metroDecorColorButton;
    }
    
    public void updateFontItems(MetroLabel element){
        metroFontColorButton.setValue((Color)element.getFill());
        metroFontSizeComboBox.setValue(Double.toString(element.getFontSize()));
        metroFontStyleComboBox.setValue(element.getFontStyle());
        /*
        metroFontColorButton = new ColorPicker();
        metroFontToolbarPart1.getChildren().addAll(metroFontLabel, metroFontColorButton);
        metroFontToolbarPart2 = new HBox();
        metroBoldButton = gui.initChildButton(metroFontToolbarPart2, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), false);
        metroItalicsButton = gui.initChildButton(metroFontToolbarPart2, ITALICS_ICON.toString(), ITALICS_TOOLTIP.toString(), false);
        metroFontSizeComboBox = new ComboBox<String>(fontSizeOptions);
        metroFontStyleComboBox = new ComboBox<String>(fontStyleOptions);
        metroFontToolbarPart2.getChildren().addAll(
                metroFontSizeComboBox, 
                metroFontStyleComboBox
        );
        */
    }
}