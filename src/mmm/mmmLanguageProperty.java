/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm;

/**
 * Helps identify the icons and tooltips of the buttons
 * @author natec
 */
public enum mmmLanguageProperty {
    LINE_COLOR_ICON, 
    LINE_COLOR_TOOLTIP, 
    
    ADDLINE_ICON, 
    ADDLINE_TOOLTIP, 
    
    REMOVELINE_ICON, 
    REMOVELINE_TOOLTIP, 
    
    ADDSTATION_ICON, 
    ADDSTATION_TOOLTIP, 
    
    REMOVESTATION_ICON, 
    REMOVESTATION_TOOLTIP, 
    
    ADDSTATION_TOLINE_ICON, 
    ADDSTATION_TOLINE_TOOLTIP, 
    
    REMOVESTATION_FROMLINE_ICON, 
    REMOVESTATION_FROMLINE_TOOLTIP,
    
    LISTSTATIONS_ICON, 
    LISTSTATIONS_TOOLTIP, 
    
    STATION_COLOR_ICON, 
    STATION_COLOR_TOOLTIP,
    
    SNAPTOGRID_ICON, 
    SNAPTOGRID_TOOLTIP, 
    
    MOVELABEL_ICON, 
    MOVELABEL_TOOLTIP, 
    
    ROTATELABEL_ICON, 
    ROTATELABEL_TOOLTIP,
    
    FINDROUTE_ICON, 
    FINDROUTE_TOOLTIP, 
    
    DECOR_COLOR_ICON, 
    DECOR_COLOR_TOOLTIP, 
    
    IMAGEBACKGROUND_ICON, 
    IMAGEBACKGROUND_TOOLTIP, 
    
    ADDIMAGE_ICON,
    ADDIMAGE_TOOLTIP, 
    
    ADDLABEL_ICON, 
    ADDLABEL_TOOLTIP, 
    
    REMOVEELEMENT_ICON, 
    REMOVEELEMENT_TOOLTIP, 
    
    FONTCOLOR_ICON, 
    FONTCOLOR_TOOLTIP, 
    
    BOLD_ICON, 
    BOLD_TOOLTIP,
    
    ITALICS_ICON, 
    ITALICS_TOOLTIP, 
    
    ZOOMIN_ICON, 
    ZOOMIN_TOOLTIP, 
    
    ZOOMOUT_ICON, 
    ZOOMOUT_TOOLTIP, 
    
    MAPINCREASE_ICON,
    MAPINCREASE_TOOLTIP, 
    
    MAPDECREASE_ICON, 
    MAPDECREASE_TOOLTIP,
    
    UNDO_ICON,
    UNDO_TOOLTIP,
    
    REDO_ICON,
    REDO_TOOLTIP,
    
    ABOUT_ICON,
    ABOUT_TOOLTIP,
    
    EXPORT_ICON,
    EXPORT_TOOLTIP,
    
    METRO_ICON,
    APP_LOGO
}
