/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm;

import com.sun.scenario.Settings;
import mmm.file.mmmFiles;
import mmm.gui.mmmWorkspace;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import mmm.data.mmmData;
import static mmm.mmmLanguageProperty.METRO_ICON;
import properties_manager.PropertiesManager;


/**
 * The application class for the MetroMapMaker program
 * @author natec
 */
public class mmmApp extends AppTemplate{
    
    /**
     * Initializes all three components in proper order
     */
    @Override
    public void buildAppComponentsHook(){
        fileComponent = new mmmFiles();
        dataComponent = new mmmData(this);
        workspaceComponent = new mmmWorkspace(this);
        welcomeDialog();
    }
    
    /**
     * Where program execution begins
     * @param args 
     */
    public static void main(String[] args){
        Locale.setDefault(Locale.US);
        launch(args);
    }
    
    /**
     * Welcome dialog before application opens.
     */
    @SuppressWarnings("unchecked")
    private void welcomeDialog(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        Stage welcomeStage = new Stage();
        welcomeStage.setTitle("Welcome to Metro Map Maker!");
        BorderPane root = new BorderPane();
        //CENTER
        ImageView imageLogo = new ImageView();
        imageLogo.setImage(new Image("file:./images/"+props.getProperty(METRO_ICON.toString())));
        
        //RIGHT
        
        Text file1 = new Text();
        Text file2 = new Text();
        Text file3 = new Text();
        Text file4 = new Text();
        Text file5 = new Text();
        
        ArrayList<Text> fileArray = new ArrayList<>();
        fileArray.add(file1);
        fileArray.add(file2);
        fileArray.add(file3);
        fileArray.add(file4);
        fileArray.add(file5);

        ArrayList<File> files = new ArrayList<>();
        File file = new File("./work/");
        //Should only load JSON files
        File[] fileList = file.listFiles(new FilenameFilter(){
            public boolean accept(File dir, String name) {
                return name.endsWith("json");
            }
        });
        
        
        Arrays.sort(fileList, new Comparator(){
            public int compare(Object o1, Object o2) {
                return compare( (File)o1, (File)o2);
            }
            private int compare( File f1, File f2){
                long result = f2.lastModified() - f1.lastModified();
                if( result > 0 ){
                    return 1;
                } else if( result < 0 ){
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        
        for (File f : fileList){
            files.add(f);
            if (files.size() >= (Math.min(5, fileList.length)))
                break;
        }
        
        for (int i = 0; i < files.size(); i++){
            fileArray.get(i).setText(files.get(i).getName());
        }
        BorderPane leftPane = new BorderPane();
        VBox recentWorkBox = new VBox();
        recentWorkBox.setAlignment(Pos.CENTER);
               
        Label recentWork = new Label("Recent Work");  
        recentWork.setFont(new Font("RecentWork", 20));
        
        recentWorkBox.getChildren().add(recentWork);
        //ONE
        Label recentFile1 = new Label(fileArray.get(0).getText());
        recentFile1.setUnderline(true);
        recentFile1.setTextFill(Color.RED);
        recentFile1.setOnMouseClicked(e -> {
            welcomeStage.close();
            modLoad(files.get(0));
        });
        recentWorkBox.getChildren().add(recentFile1);
        //TWO  
        Label recentFile2 = new Label(fileArray.get(1).getText());
        recentFile2.setUnderline(true);
        recentFile2.setTextFill(Color.RED);
        recentFile2.setOnMouseClicked(e -> {
            welcomeStage.close();
            modLoad(files.get(1));
        });
        recentWorkBox.getChildren().add(recentFile2);
        //THREE
        Label recentFile3 = new Label(fileArray.get(2).getText());
        recentFile3.setUnderline(true);
        recentFile3.setTextFill(Color.RED);
        recentFile3.setOnMouseClicked(e -> {
            welcomeStage.close();
            modLoad(files.get(2));
        });
        recentWorkBox.getChildren().add(recentFile3);
        //FOUR
        Label recentFile4 = new Label(fileArray.get(3).getText());
        recentFile4.setTextFill(Color.RED);
        recentFile4.setUnderline(true);
        recentFile4.setOnMouseClicked(e -> {
            welcomeStage.close();
            modLoad(files.get(3));
        });
        recentWorkBox.getChildren().add(recentFile4);
        //FIVE
        Label recentFile5 = new Label(fileArray.get(4).getText());
        recentFile5.setUnderline(true);
        recentFile5.setTextFill(Color.RED);
        recentFile5.setOnMouseClicked(e -> {
            welcomeStage.close();
            modLoad(files.get(4));
        });
        recentWorkBox.getChildren().add(recentFile5);
        
        leftPane.setCenter(recentWorkBox);
        
        
        //BOTTOM
        
        Label createNewMap = new Label("Create New Map!");
        createNewMap.setOnMouseClicked(e->{
            welcomeStage.close();
            Alert newDialog = new Alert(AlertType.CONFIRMATION);
            newDialog.setTitle("NEW MAP CREATION");
            newDialog.setContentText("Please prepare new map name: ");
            newDialog.showAndWait();
                                        

                //String currentText = this.getText();

                // Traditional way to get the response value.
                
                TextInputDialog dialog = new TextInputDialog();
                dialog.setTitle("Text Input Dialog");
                dialog.setHeaderText(null);
                dialog.setContentText("Please enter your new text: ");
                File theDir = new File(PATH_WORK + "PLACEHOLDER");
               
                    // Traditional way to get the response value.
                    Optional<String> result = dialog.showAndWait();
                    theDir = new File(PATH_WORK + result.get());
                    if(!theDir.exists()){
                        System.out.println("creating directory: " + theDir.getName());
                        boolean complete = false;

                        try{
                            theDir.mkdir();
                            File mapFile = new File(PATH_WORK + result.get() + "/" + result.get() + ".mmm");
                            ((mmmData)dataComponent).setFileName(result.get());
                            mapFile.createNewFile();
                            complete = true;
                        } 
                        catch(IOException ie){
                            //handle it
                        }
                        catch(SecurityException se){
                            //handle it
                        }        
                        if(complete) {    
                            System.out.println("DIR created");  
                        }
                    }
                    else{
                        dialog = new TextInputDialog();
                        dialog.setTitle("Folder Exists!");
                        dialog.setHeaderText(null);
                        dialog.setContentText("Please enter a unique name: ");
                    }
                
        });
        createNewMap.setTextFill(Color.web("#0000FF"));
        
        root.setCenter(imageLogo);
        root.setLeft(leftPane);
        root.setMargin(leftPane, new Insets(0,100,0,0));
        //Create New Map
        root.setBottom(createNewMap);
        root.setMargin(createNewMap, new Insets(0,0,140,0));
        root.setAlignment(createNewMap, Pos.CENTER);

        
        Scene welcome = new Scene(root, 1100, 600);
        welcomeStage.setScene(welcome);
        welcomeStage.showAndWait();
        
    }
    private void newAugment(){
        
             
    }
    private void modLoad(File loadFile){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
                // RESET THE WORKSPACE
		this.getWorkspaceComponent().resetWorkspace();

                // RESET THE DATA
                this.getDataComponent().resetData();
                
                System.out.println(loadFile.getPath());
                
                // LOAD THE FILE INTO THE DATA
                this.getFileComponent().loadData(this.getDataComponent(), loadFile.getPath());
                
		// MAKE SURE THE WORKSPACE IS ACTIVATED
                this.getWorkspaceComponent().activateWorkspace(this.getGUI().getAppPane());
                
                // AND MAKE SURE THE FILE BUTTONS ARE PROPERLY ENABLED
                this.getGUI().updateToolbarControls(false);
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                e.printStackTrace();
                //dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            }
    }
}
