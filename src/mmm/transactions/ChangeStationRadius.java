/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroStation;


/**
 *
 * @author natec
 */
public class ChangeStationRadius implements jTPS_Transaction{

    double newRadius;
    double oldRadius;
    MetroStation currentStation;
    double oldX;
    double oldY;
    double newX;
    double newY;
    
    public ChangeStationRadius(double newRadius, double oldRadius, MetroStation currentStation){
        this.newRadius = newRadius;
        this.oldRadius = oldRadius;
        this.currentStation = currentStation;
        //this.oldX = currentStation.getStationLabel().getX();
        //this.oldX = currentStation.getStationLabel().getY();
        //this.newX = 
    }
    
    @Override
    public void doTransaction() {
        currentStation.setRadiusX(newRadius);
        currentStation.setRadiusY(newRadius);
        currentStation.getStationLabel().moveLabel();
    }

    @Override
    public void undoTransaction() {
        currentStation.setRadiusX(oldRadius);
        currentStation.setRadiusY(oldRadius);
        currentStation.getStationLabel().moveLabel();
    }
    
}
