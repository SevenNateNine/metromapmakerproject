/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroLabel;
import mmm.data.MetroStation;

/**
 *
 * @author natec
 */
public class MoveLabel implements jTPS_Transaction{

    MetroLabel label;
    
    public MoveLabel(MetroLabel label){
        this.label = label;
    }
    
    @Override
    public void doTransaction() {
        label.moveLabelClockWise();
    }

    @Override
    public void undoTransaction() {
        label.moveLabelCounterClockWise();
    }
    
}
