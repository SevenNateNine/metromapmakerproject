/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroLabel;

/**
 *
 * @author natec
 */
public class BoldText implements jTPS_Transaction{

    MetroLabel label;
    
    public BoldText(MetroLabel label){
        this.label = label;
    }
    
    @Override
    public void doTransaction() {
        label.boldText();
    }

    @Override
    public void undoTransaction() {
        label.boldText();
    }
    
}
