/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroLabel;

/**
 *
 * @author natec
 */
public class ChangeFontSize implements jTPS_Transaction{
    
    MetroLabel metroLabel;
    double oldFontSize;
    double newFontSize;
    
    /**
     * Constructor for the ChangeFont transaction class
     */
    public ChangeFontSize(MetroLabel metroLabel, double newFontSize){
        this.metroLabel = metroLabel;
        this.oldFontSize = metroLabel.getFontSize();
        this.newFontSize = newFontSize;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        metroLabel.setFontSize(newFontSize);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        metroLabel.setFontSize(oldFontSize);
    }
    
}