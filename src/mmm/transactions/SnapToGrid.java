/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroStation;

/**
 *
 * @author natec
 */
public class SnapToGrid implements jTPS_Transaction{
    
    MetroStation currentStation;
    double oldX;
    double oldY;
    double newX;
    double newY;
    
    public SnapToGrid(MetroStation currentStation){
        this.currentStation = currentStation;
        oldX = currentStation.getCenterX();
        oldY = currentStation.getCenterY();
        newX = (long)(oldX - oldX%20);
        newY = (long)(oldX - oldY%20);
    }
    
    @Override
    public void doTransaction() {
        currentStation.setLocation(newX, newY);
        currentStation.getStationLabel().setLocation(
                newX + currentStation.getRadiusX(), 
                newY - currentStation.getRadiusY()
        );
    }

    @Override
    public void undoTransaction() {
        currentStation.setLocation(oldX, oldY);
        currentStation.getStationLabel().setLocation(
                oldX + currentStation.getRadiusX(), 
                oldY - currentStation.getRadiusY()
        );
    }
}
