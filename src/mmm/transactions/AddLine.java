/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 * Creates an AddLine transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class AddLine implements jTPS_Transaction{
    
    Shape line;
    Shape lineSegment;
    Shape lineEndOne;
    Shape lineEndTwo;
    Shape lineLabelOne;
    Shape lineLabelTwo;
    
    ObservableList<Node> elementArray;
    
    /**
     * Constructor for the AddLine transaction class
     */
    public AddLine(ObservableList<Node> elements, Shape newLine, Shape newLineSegment,
            Shape newLineEnd, Shape secondNewLineEnd, Shape newLineLabel,
            Shape secondNewLineLabel){
        elementArray = elements;
        this.line = newLine;
        this.lineSegment = newLineSegment;
        this.lineEndOne = newLineEnd;
        this.lineEndTwo = secondNewLineEnd;
        this.lineLabelOne = newLineLabel;
        this.lineLabelTwo = secondNewLineLabel;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elementArray.add(line);
        elementArray.add(lineSegment);
        elementArray.add(lineEndOne);
        elementArray.add(lineEndTwo);
        elementArray.add(lineLabelOne);
        elementArray.add(lineLabelTwo);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        elementArray.remove(line);
        elementArray.remove(lineSegment);
        elementArray.remove(lineEndOne);
        elementArray.remove(lineEndTwo);
        elementArray.remove(lineLabelOne);
        elementArray.remove(lineLabelTwo);
    }
}
