/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroLine;

/**
 * Creates an ChangeLineThickness transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class ChangeLineThickness implements jTPS_Transaction{
    
    double newThickness;
    double oldThickness;
    MetroLine currentLine;
    
    /**
     * Constructor for the ChangeLineThickness transaction class
     */
    public ChangeLineThickness(double newThickness, double oldThickness, MetroLine currentLine){
        this.newThickness = newThickness;
        this.oldThickness = oldThickness;
        this.currentLine = currentLine;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        currentLine.setLineThickness(newThickness);
    }

    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        currentLine.setLineThickness(oldThickness);
    }
    
}
