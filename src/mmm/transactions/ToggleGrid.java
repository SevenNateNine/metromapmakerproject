/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import java.util.Iterator;
import java.util.ListIterator;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import jtps.jTPS_Transaction;
import mmm.data.MetroLineSegment;

//Need to get dimensions of FULL SIZE CANVAS

/**
 *
 * @author natec
 */
public class ToggleGrid implements jTPS_Transaction{
    
    ObservableList<Node> elementArray;
    BorderPane canvas;
    double canvasHeight;
    double canvasWidth;
    boolean isOn;
    
    public ToggleGrid(BorderPane canvas, ObservableList<Node> elementArray, boolean isOn){
        this.canvas = canvas;
        this.elementArray = elementArray;
        canvasHeight = canvas.getHeight();
        canvasWidth = canvas.getWidth();
        this.isOn = isOn;
    }
    
    @Override
    public void doTransaction() {
        if(!isOn){
            drawLines();
        }
        if(isOn){
            removeLines();
        }
    }

    @Override
    public void undoTransaction() {
         if(!isOn){
            removeLines();
        }
        if(isOn){
            drawLines();           
        }    
    }
    
    public void drawLines(){
        double w = 4000;
        while(w >= -100){
            Line newLine = new Line(w, 4000, w, -100);
            newLine.setOpacity(.3);
            elementArray.add(newLine);
            w = w - 20; //Every 10 make a new line
        }
        //Horizontal lines after
        double h = 4000;
        while(h >= -100){
            Line newLine = new Line(4000, h, -100, h); 
            newLine.setOpacity(.3);
            elementArray.add(newLine);
            h = h - 20;
        }
    }
    
    public void removeLines(){
        ListIterator litr = elementArray.listIterator();
        while(litr.hasNext()){
            Object next = litr.next();
            if(next instanceof Line){
                if(!(next instanceof MetroLineSegment)){
                    litr.remove();
                }
            }
        } 
    }
}
