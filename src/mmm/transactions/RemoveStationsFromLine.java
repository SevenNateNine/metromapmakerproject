/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import jtps.jTPS_Transaction;
import mmm.data.MetroLineSegment;
import mmm.data.MetroStation;

/**
 * Creates an RemoveStationsFromLine transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class RemoveStationsFromLine implements jTPS_Transaction{
    
    ObservableList<Node> elements;
    MetroLineSegment lineSegmentOne;
    MetroLineSegment lineSegmentTwo;
    MetroLineSegment lineSegmentNew;
    MetroStation removedStation;
    
    /**
     * Constructor for the RemoveStationsFromLine transaction class
     */
    public RemoveStationsFromLine(ObservableList<Node> elements,
            MetroLineSegment lineSegmentOne, MetroLineSegment lineSegmentTwo,
            MetroLineSegment lineSegmentNew, MetroStation removedStation){
        this.elements = elements;
        this.lineSegmentOne = lineSegmentOne;
        this.lineSegmentTwo = lineSegmentTwo;
        this.lineSegmentNew = lineSegmentNew;
        this.removedStation = removedStation;
    }

    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elements.remove(lineSegmentOne);
        elements.remove(lineSegmentTwo);
        elements.add(lineSegmentNew);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
       elements.add(lineSegmentOne);
       elements.add(lineSegmentTwo);
       elements.remove(lineSegmentNew);
    }
}
