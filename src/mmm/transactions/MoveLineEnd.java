/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;

/**
 * Creates a MoveLineEnd transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class MoveLineEnd implements jTPS_Transaction{
    
    /**
     * Constructor for the MoveLineEnd transaction class
     */
    public MoveLineEnd(){
        
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
