/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import mmm.data.MetroLabel;

/**
 *
 * @author natec
 */
public class ChangeFontColor implements jTPS_Transaction{
    
    MetroLabel label;
    Color oldColor;
    Color newColor;
    
    public ChangeFontColor(MetroLabel label, Color newColor){
        this.label = label;
        this.oldColor = (Color)label.getFill();
        this.newColor = newColor;
    }
    
    @Override
    public void doTransaction() {
        label.setFill(newColor);
    }

    @Override
    public void undoTransaction() {
        label.setFill(oldColor);
    }
}
