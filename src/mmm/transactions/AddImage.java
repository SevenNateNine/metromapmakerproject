/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 * Creates an AddImage transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class AddImage implements jTPS_Transaction{
    
    Shape shape;
    ObservableList<Node> elements;
    
    /**
     * Constructor for the AddImage transaction class
     */
    public AddImage(ObservableList<Node> elements, Shape shape){
        this.elements = elements;
        this.shape = shape;
    }

    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elements.add(shape);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        elements.remove(shape);
    }
}
