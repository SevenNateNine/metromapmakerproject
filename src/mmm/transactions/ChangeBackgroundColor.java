/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 * Creates an ChangeBackgroundColor transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class ChangeBackgroundColor implements jTPS_Transaction{

    Pane newCanvas;
    Background oldBackground;
    Background newBackground;
    
    /**
     * Constructor for the ChangeBackgroundColor transaction class
     */
    public ChangeBackgroundColor(Pane newCanvas, Background oldBackground, Background newBackground){
        this.newCanvas = newCanvas;
        this.oldBackground = oldBackground;
        this.newBackground = newBackground;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        newCanvas.setBackground(newBackground);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        newCanvas.setBackground(oldBackground);
    }
    
}
