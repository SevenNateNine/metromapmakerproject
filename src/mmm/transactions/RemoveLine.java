/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import java.util.LinkedList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import mmm.data.MetroLineSegment;

/**
 * Creates an RemoveLine transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class RemoveLine implements jTPS_Transaction{
    
    ObservableList<Node> elements;
    LinkedList<MetroLineSegment> lineSegments;
    Shape line;
    Shape lineEndOne;
    Shape lineEndTwo;
    Shape endLabelOne;
    Shape endLabelTwo;
    
    /**
     * Constructor for the RemoveLine transaction class
     */
    
    public RemoveLine(ObservableList<Node> elements, Shape line, 
            LinkedList<MetroLineSegment> lineSegments,
            Shape lineEndOne, Shape lineEndTwo, Shape endLabelOne,
            Shape endLabelTwo){
        this.elements = elements;
        this.line = line;
        this.lineSegments = lineSegments;
        this.lineEndOne = lineEndOne;
        this.lineEndTwo = lineEndTwo;
        this.endLabelOne = endLabelOne;
        this.endLabelTwo = endLabelTwo;
    }
    
    /**
     * Performs the transaction
     */
    
    @Override
    public void doTransaction() {
        for(int i = 0; i < lineSegments.size(); i++){
            elements.remove(lineSegments.get(i));
        }
        elements.remove(line);
        elements.remove(endLabelOne);
        elements.remove(endLabelTwo);
        elements.remove(lineEndOne);
        elements.remove(lineEndTwo);
        
    }

    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        for(int i = 0; i < lineSegments.size(); i++){
            elements.add(lineSegments.get(i));
        }
        elements.add(line);
        elements.add(lineEndOne);
        elements.add(lineEndTwo);
        elements.add(endLabelOne);
        elements.add(endLabelTwo);
    }
    
}
