/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author natec
 */
public class ChangeMapSize implements jTPS_Transaction{
    
    Pane width;
    Pane height;
    Pane canvas;
    boolean isIncrease;
    
    
    public ChangeMapSize(BorderPane canvas, boolean isIncrease){
        this.width = (Pane)canvas.getBottom();
        this.height = (Pane)canvas.getRight();
        this.canvas = (Pane)((ScrollPane)canvas.getCenter()).getContent();
        this.isIncrease = isIncrease;
    }
    
    @Override
    public void doTransaction() {
        if(isIncrease){
            canvas.setPrefSize(
                    canvas.getHeight() + canvas.getHeight()/10,
                    canvas.getWidth() + canvas.getWidth()/10
                    );
            height.setPrefWidth(
                    height.getWidth() - canvas.getWidth()/10
            );
            width.setPrefHeight(
                    width.getHeight() - canvas.getHeight()/10
            );
        }
        else{
            canvas.setPrefSize(
                    canvas.getHeight() - canvas.getHeight()/10,
                    canvas.getWidth() - canvas.getWidth()/10
                    );
            height.setPrefWidth(
                    height.getWidth() + canvas.getWidth()/10
            );
            width.setPrefHeight(
                    width.getHeight() + canvas.getHeight()/10
            );
        }
    }

    @Override
    public void undoTransaction() {
        if(isIncrease){
            canvas.setPrefSize(
                    canvas.getHeight() - canvas.getHeight()/10,
                    canvas.getWidth() - canvas.getWidth()/10
                    );
            height.setPrefWidth(
                    height.getWidth() + canvas.getWidth()/10
            );
            width.setPrefHeight(
                    width.getHeight() + canvas.getHeight()/10
            );
        }
        else{
            canvas.setPrefSize(
                    canvas.getHeight() + canvas.getHeight()/10,
                    canvas.getWidth() + canvas.getWidth()/10
                    );
            height.setPrefWidth(
                    height.getWidth() - canvas.getWidth()/10
            );
            width.setPrefHeight(
                    width.getHeight() - canvas.getHeight()/10
            );
        }
    }
}
    
