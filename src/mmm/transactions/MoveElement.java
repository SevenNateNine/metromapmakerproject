/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import mmm.data.MetroElement;

/**
 * Creates an MoveElement transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class MoveElement implements jTPS_Transaction{
    
    double startX;
    double startY;
    double endX;
    double endY;
    MetroElement shape;
    
    /**
     * Construct for the MoveElement transaction class
     */
    public MoveElement(MetroElement shape, double startX, double startY, double endX, double endY){
        this.shape = shape;
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
       shape.start((int)endX, (int)endY);
    }
    
    /**
     * Undoes the transaction 
     */
    @Override
    public void undoTransaction() {
       shape.start((int)startX, (int)startY);
    }
    
}
