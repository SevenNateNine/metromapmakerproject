/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import jtps.jTPS_Transaction;

/**
 *
 * @author natec
 */
public class ChangeMapZoom implements jTPS_Transaction{
    
    ScrollPane canvas;
    Node content;
    Group contentGroup;
    Group zoomGroup;
    Scale scaleTransform;
    boolean isIncrease;
    
    public ChangeMapZoom(ScrollPane canvas, boolean isIncrease){
        this.canvas = canvas;
        this.content = canvas.getContent();
        this.isIncrease = isIncrease;
        contentGroup = new Group();
        zoomGroup = new Group();
        
        contentGroup.getChildren().add(zoomGroup);
        zoomGroup.getChildren().add(content);
        Pane newPane = new Pane();
        newPane.getChildren().add(contentGroup);
        canvas.setContent(newPane);
        
    }
    
    /*
    this.content = content;
    Group contentGroup = new Group();
    zoomGroup = new Group();
    contentGroup.getChildren().add(zoomGroup);
    zoomGroup.getChildren().add(content);
    setContent(contentGroup);
    scaleTransform = new Scale(scaleValue, scaleValue, 0, 0);
    zoomGroup.getTransforms().add(scaleTransform);
    */
    
    @Override
    public void doTransaction() {
        if(isIncrease){
            scaleTransform = new Scale(1.10, 1.10, 0, 0);        
        }
        else{
            scaleTransform = new Scale(.90, .90, 0, 0);
        }
        zoomGroup.getTransforms().add(scaleTransform);
    }

    @Override
    public void undoTransaction() {
        if(isIncrease){
            scaleTransform = new Scale(.90, .90, 0, 0);
        }
        else{
            scaleTransform = new Scale(1.10, 1.10, 0, 0);
        }
        zoomGroup.getTransforms().add(scaleTransform);
    }
}
