/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroLabel;

/**
 *
 * @author natec
 */
public class ItalicizeText implements jTPS_Transaction{
    MetroLabel label;
    
    public ItalicizeText(MetroLabel label){
        this.label = label;
    }
    
    @Override
    public void doTransaction() {
        label.italicizeText();
    }

    @Override
    public void undoTransaction() {
        label.italicizeText();
    }
}
