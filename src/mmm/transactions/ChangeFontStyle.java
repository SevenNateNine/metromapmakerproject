/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import jtps.jTPS_Transaction;
import mmm.data.MetroLabel;

/**
 * Creates a ChangeFontStyle transaction to be added to the jTPS transaction 
 array for undo/redo
 * @author natec
 */
public class ChangeFontStyle implements jTPS_Transaction{
    
    MetroLabel metroLabel;
    String oldFontStyle;
    String newFontStyle;
    
    /**
     * Constructor for the ChangeFont transaction class
     */
    public ChangeFontStyle(MetroLabel metroLabel, String newFontStyle){
        this.metroLabel = metroLabel;
        this.oldFontStyle = metroLabel.getFontStyle();
        this.newFontStyle = newFontStyle;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        metroLabel.setFontStyle(newFontStyle);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        metroLabel.setFontStyle(oldFontStyle);
    }
    
}
