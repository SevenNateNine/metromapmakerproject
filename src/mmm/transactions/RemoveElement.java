/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 * Creates an RemoveElement transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class RemoveElement implements jTPS_Transaction{

    ObservableList<Node> elements;
    Shape element;
    
    /**
     * Constructor for the RemoveElement transaction class
     */
    public RemoveElement(ObservableList<Node> elements, Shape element){
        this.elements = elements;
        this.element = element;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elements.remove(element);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        elements.add(element);
    }
    
}
