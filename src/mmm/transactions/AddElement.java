/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import mmm.data.MetroElement;

/**
 * Creates an AddLine transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class AddElement implements jTPS_Transaction{
    
    Shape element;
    ObservableList<Node> elementArray;
    
    /**
     * Constructor for the AddLine transaction class
     */
    public AddElement(ObservableList<Node> elements, Shape newElement){
        elementArray = elements;
        element = newElement;
    }
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elementArray.add(element);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
         elementArray.remove(element);
    }
}
