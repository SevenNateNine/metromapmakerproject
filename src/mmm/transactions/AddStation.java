/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 * Creates an AddStation transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class AddStation implements jTPS_Transaction{

    Shape element;
    Shape secondElement;
    ObservableList<Node> elementArray;
    
    /**
     * Constructor for the AddStation transaction class
     */
    public AddStation(ObservableList<Node> elements, Shape newElement, Shape newElement2){
        elementArray = elements;
        element = newElement;
        secondElement = newElement2;
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elementArray.add(element);
        elementArray.add(secondElement);
    }

    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        elementArray.remove(element);
        elementArray.remove(secondElement);
    }
    
}
