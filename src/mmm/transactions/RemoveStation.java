/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;


/**
 * Creates an RemoveStation transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class RemoveStation implements jTPS_Transaction{
    
    ObservableList<Node> elements;
    Shape station;
    Shape stationLabel;
    
     /**
     * Constructor for the RemoveStation transaction class
     */
    
    public RemoveStation(ObservableList<Node> elements, Shape station,
            Shape stationLabel){
        this.elements = elements;
        this.station = station;
        this.stationLabel = stationLabel;
    }

    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {
        elements.remove(station);
        elements.remove(stationLabel);
    }
    
    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        elements.add(station);
        elements.add(stationLabel);
    }
}
