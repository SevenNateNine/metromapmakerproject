/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import jtps.jTPS_Transaction;
import mmm.data.MetroLine;
import mmm.data.MetroLineSegment;
import mmm.data.MetroStation;

/**
 * Creates an AddStationsToLine transaction to be added to the jTPS transaction 
 * array for undo/redo
 * @author natec
 */
public class AddStationsToLine implements jTPS_Transaction{

    ObservableList<Node> elements;
    MetroLineSegment closestLineSegment;
    MetroLineSegment newLineSegment;
    MetroStation station;
    MetroLine masterLine;
    double stationX;
    double stationY;
    MetroStation removedLineEnd;
    
    
    /**
     * Constructor for the AddStationsToLine transaction class
     */
    public AddStationsToLine(ObservableList<Node> elements,
            MetroLineSegment closestLineSegment,
            MetroStation station){
        this.elements = elements;
        this.closestLineSegment = closestLineSegment;
        this.station = station;
        this.masterLine = closestLineSegment.getMasterLine();
            stationX = station.getX();
            stationY = station.getY();
            this.removedLineEnd = closestLineSegment.getSecondLineEnd();
            
            newLineSegment = new MetroLineSegment(
                    closestLineSegment.getLineColor(), 
                    station, 
                    closestLineSegment.getSecondLineEnd(), 
                    closestLineSegment.getMasterLine(),
                    closestLineSegment.getX(), 
                    closestLineSegment.getY(), 
                    closestLineSegment.getSecondLineEnd().getCenterX(), 
                    closestLineSegment.getSecondLineEnd().getCenterY()
                );
    }
    
    /**
     * Performs the transaction
     */
    @Override
    public void doTransaction() {        
            masterLine.getStations().add(station);
            station.setInLine(true);
            station.setMasterLine(closestLineSegment.getMasterLine());
            
                //Should be done within the method
                closestLineSegment.getSecondLineEnd().centerXProperty().unbind();
                closestLineSegment.getSecondLineEnd().centerYProperty().unbind();
                newLineSegment.getSecondLineEnd().setLocation(
                        newLineSegment.getEndX(), 
                        newLineSegment.getEndY()
                );
            closestLineSegment.addStationToLine(station, newLineSegment);
        
        elements.add(newLineSegment);
        //Have to account for changed locations for both line segments, and station
    }

    /**
     * Undoes the transaction
     */
    @Override
    public void undoTransaction() {
        masterLine.getStations().remove(station);
        station.setInLine(false);
        station.setMasterLine(null);
        
        closestLineSegment.endXProperty().unbind();
        closestLineSegment.endYProperty().unbind();
        closestLineSegment.setEndX(removedLineEnd.getCenterX());
        closestLineSegment.setEndY(removedLineEnd.getCenterY());
        closestLineSegment.endXProperty().bind(removedLineEnd.centerXProperty());
        closestLineSegment.endYProperty().bind(removedLineEnd.centerYProperty());
        
        closestLineSegment.setSecondLineEnd(removedLineEnd);
        
        closestLineSegment.removeStationFromLine(station, newLineSegment);
        
        station.setLocation(stationX, stationY);
        elements.remove(newLineSegment);
    }
    
}
